import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {UserListComponent} from './components/user-list/user-list.component';
import {UserFormComponent} from './components/user-form/user-form.component';
import {UserProfileComponent} from "./components/user-profile/user-profile.component";
import {LoginComponent} from "./components/login/login.component";
import {AuthGuardService as AuthGuard} from "./service/auth-guard-service/auth-guard.service";
import {AuthGuardLoggedinService as AuthGuardLoggedin} from "./service/auth-guard-loggedin-service/auth-guard-loggedin.service";
import {RoleGuardService as RoleGuard} from "./service/role-guard-service/role-guard.service";
import {EmergencyContactListComponent} from "./components/emergency-contact-list/emergency-contact-list.component";
import {EmergencyContactFormComponent} from "./components/emergency-contact-form/emergency-contact-form.component";
import {EmergencyContactProfileComponent} from "./components/emergency-contact-profile/emergency-contact-profile.component";
import {HomeComponent} from "./components/home/home.component";
import {EmergencyRequestFormComponent} from "./components/emergency-request-form/emergency-request-form.component";
import {ForgottenPasswordTelNumComponent} from "./components/forgotten-password-tel-num/forgotten-password-tel-num.component";
import {ForgottenPasswordChangeComponent} from "./components/forgotten-password-change/forgotten-password-change.component";
import {EmergencyRequestListComponent} from "./components/emergency-request-list/emergency-request-list.component";
import {FriendListComponent} from "./components/friend-list/friend-list.component";
import {FriendChatComponent} from "./components/friend-chat/friend-chat.component";

const routes: Routes = [
  {path: 'users', component: UserListComponent, canActivate: [RoleGuard], data: {expectedRole: "ROLE_ADMIN"}},
  {path: 'addUser', component: UserFormComponent, canActivate: [AuthGuardLoggedin]},
  {path: 'users/:id', component: UserProfileComponent, canActivate: [AuthGuard]},
  {path: 'emergencyContacts', component: EmergencyContactListComponent, canActivate: [AuthGuard]},
  {path: 'emergencyContacts/:id', component: EmergencyContactProfileComponent, canActivate: [AuthGuard]},
  {path: 'addEmergencyContact', component: EmergencyContactFormComponent, canActivate: [AuthGuard]},
  {path: 'emergencyRequests', component: EmergencyRequestListComponent, canActivate: [AuthGuard]},
  {path: 'addEmergencyRequest', component: EmergencyRequestFormComponent, canActivate: [AuthGuard]},
  {path: 'friends', component: FriendListComponent, canActivate: [AuthGuard]},
  {path: 'chat', component: FriendChatComponent, canActivate: [AuthGuard]},
  {path: 'login', component: LoginComponent, canActivate: [AuthGuardLoggedin]},
  {path: 'forgottenPassword', component: ForgottenPasswordTelNumComponent, canActivate: [AuthGuardLoggedin]},
  {path: 'modifyPassword', component: ForgottenPasswordChangeComponent, canActivate: [AuthGuardLoggedin]},
  {path: '', component: HomeComponent, canActivate: [AuthGuard]},
  {path: '**', redirectTo: '/'}
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule {
}
