import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {RouterModule} from "@angular/router";
import {UserListComponent} from './components/user-list/user-list.component';
import {UserFormComponent} from './components/user-form/user-form.component';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {UserServiceService} from "./service/user-service/user-service.service";
import {FooterComponent} from './components/footer/footer.component';
import {MenuComponent} from './components/menu/menu.component';
import {UserProfileComponent} from './components/user-profile/user-profile.component';
import {LoginComponent} from './components/login/login.component';
import {AuthServiceService} from "./service/auth-service/auth-service.service";
import {AuthGuardService} from "./service/auth-guard-service/auth-guard.service";
import {RoleGuardService} from "./service/role-guard-service/role-guard.service";
import {AuthGuardLoggedinService} from "./service/auth-guard-loggedin-service/auth-guard-loggedin.service";
import {EmergencyContactListComponent} from './components/emergency-contact-list/emergency-contact-list.component';
import {EmergencyContactService} from "./service/emergency-contact-service/emergency-contact.service";
import {EmergencyContactFormComponent} from './components/emergency-contact-form/emergency-contact-form.component';
import {EmergencyContactProfileComponent} from './components/emergency-contact-profile/emergency-contact-profile.component';
import {HomeComponent} from './components/home/home.component';
import {EmergencyRequestFormComponent} from './components/emergency-request-form/emergency-request-form.component';
import {EmergencyRequestListComponent} from './components/emergency-request-list/emergency-request-list.component';
import {EmergencyRequestService} from "./service/emergency-request-service/emergency-request.service";
import {DatePipe} from "@angular/common";
import {ForgottenPasswordTelNumComponent} from './components/forgotten-password-tel-num/forgotten-password-tel-num.component';
import {ForgottenPasswordChangeComponent} from './components/forgotten-password-change/forgotten-password-change.component';
import {GeolocationComponent} from './components/geolocation/geolocation.component';
import {AllergyListComponent} from './components/allergy-list/allergy-list.component';
import {SicknessListComponent} from './components/sickness-list/sickness-list.component';
import {AllergyService} from "./service/allergy-service/allergy.service";
import {SicknessService} from "./service/sickness-service/sickness.service";
import {AllergyFormComponent} from './components/allergy-form/allergy-form.component';
import {SicknessFormComponent} from './components/sickness-form/sickness-form.component';
import {EmergencyRequestPipe} from './pipes/emergency-request-pipe/emergency-request.pipe';
import {EmergencyContactPipe} from './pipes/emergency-contact-pipe/emergency-contact.pipe';
import {WebSocketService} from "./service/web-socket-service/web-socket.service";
import {NotificationComponent} from './components/notification/notification.component';
import {FriendListComponent} from './components/friend-list/friend-list.component';
import {FriendChatComponent} from './components/friend-chat/friend-chat.component';
import {FriendService} from "./service/friend-service/friend.service";
import {FriendsPipe} from './pipes/friends-pipe/friends.pipe';
import {MessageService} from "./service/message-service/message.service";
import {ImageService} from "./service/image-service/image.service";

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserFormComponent,
    FooterComponent,
    MenuComponent,
    UserProfileComponent,
    LoginComponent,
    EmergencyContactListComponent,
    EmergencyContactFormComponent,
    EmergencyContactProfileComponent,
    HomeComponent,
    EmergencyRequestFormComponent,
    EmergencyRequestListComponent,
    ForgottenPasswordTelNumComponent,
    ForgottenPasswordChangeComponent,
    GeolocationComponent,
    AllergyListComponent,
    SicknessListComponent,
    AllergyFormComponent,
    SicknessFormComponent,
    EmergencyRequestPipe,
    EmergencyContactPipe,
    NotificationComponent,
    FriendListComponent,
    FriendChatComponent,
    FriendsPipe,
  ],
  imports: [
    BrowserModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [UserServiceService, AuthServiceService, AuthGuardService, RoleGuardService,
    AuthGuardLoggedinService, EmergencyContactService, EmergencyRequestService, DatePipe,
    AllergyService, SicknessService, EmergencyRequestPipe, WebSocketService, FriendService, FriendsPipe,
    MessageService, ImageService],
  bootstrap: [AppComponent]
})

export class AppModule {
}
