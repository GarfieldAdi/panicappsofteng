import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {User} from "../../model/user";
import {Credentials} from "../../model/credentials";
import {AuthServiceService} from "../auth-service/auth-service.service";
import {EmergencyContact} from "../../model/emergencyContact";

@Injectable()
export class EmergencyContactService {

  emContactUrl: string;
  credentials: Credentials;

  constructor(private http: HttpClient, private authService: AuthServiceService) {
    this.emContactUrl = 'http://localhost:8080/emergencyContacts';
    this.credentials = authService.getCredentials();
  }

  public findAllWithID(): Observable<EmergencyContact[]> {
    return this.http.get<EmergencyContact[]>(this.emContactUrl + '/' + this.credentials.id);
  }

  public delete(emContact: EmergencyContact) {
    return this.http.request<EmergencyContact>('delete', this.emContactUrl, {body: emContact});
  }

  public save(emContact: EmergencyContact) {
    return this.http.post<EmergencyContact>(this.emContactUrl, emContact);
  }

  public update(emContact: EmergencyContact) {
    return this.http.put<User>(this.emContactUrl, emContact);
  }

  public findEmContact(emContactID: number) {
    return this.http.get<EmergencyContact>(this.emContactUrl + "/byID/" + emContactID.toString());
  }

}
