import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {PasswordChange} from "../../model/passwordChange";
import {User} from "../../model/user";
import {forkJoin} from "rxjs/observable/forkJoin";
import {DomSanitizer} from "@angular/platform-browser";

@Injectable()
export class ImageService {

  private readonly imagesUrl: string;

  constructor(private http: HttpClient, private sanitizer: DomSanitizer) {
    this.imagesUrl = 'http://localhost:8080/images';
  }

  public getFile(fileName: string) {
    return this.http.get(this.imagesUrl + "/" + fileName);
  }

  public getAllImagesOfFriends(friends: User[]) {
    let imagePromises = [];
    friends.forEach(friend => {
      imagePromises.push(this.getFile(friend.fileName));
    })
    return forkJoin(imagePromises);
  }

  public getFileFromEncoded(data: any) {
    return this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + data["content"])
  }

  public getFilesFromEncoded(data: any[]) {
    let images = [];
    data.forEach(encodedImage => {
      images.push(this.sanitizer.bypassSecurityTrustUrl('data:image/jpeg;base64,' + encodedImage["content"]));
    })
    return images;
  }

}
