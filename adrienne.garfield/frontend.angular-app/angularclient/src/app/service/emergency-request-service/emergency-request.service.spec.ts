import { TestBed, inject } from '@angular/core/testing';

import { EmergencyRequestService } from './emergency-request.service';

describe('EmergencyRequestService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [EmergencyRequestService]
    });
  });

  it('should be created', inject([EmergencyRequestService], (service: EmergencyRequestService) => {
    expect(service).toBeTruthy();
  }));
});
