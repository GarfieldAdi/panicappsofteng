import {Injectable} from '@angular/core';
import {Credentials} from "../../model/credentials";
import {HttpClient} from "@angular/common/http";
import {AuthServiceService} from "../auth-service/auth-service.service";
import {Observable} from "rxjs";
import {EmergencyRequest} from "../../model/emergencyRequest";
import {User} from "../../model/user";

@Injectable()
export class EmergencyRequestService {

  emRequestUrl: string;
  credentials: Credentials;

  constructor(private http: HttpClient, private authService: AuthServiceService) {
    this.emRequestUrl = 'http://localhost:8080/emergencyRequests';
    this.credentials = authService.getCredentials();
  }

  public findAllWithID(): Observable<EmergencyRequest[]> {
    return this.http.get<EmergencyRequest[]>(this.emRequestUrl + '/' + this.credentials.id);
  }

  public delete(emRequest: EmergencyRequest) {
    return this.http.request<EmergencyRequest>('delete', this.emRequestUrl, {body: emRequest});
  }

  public save(emRequest: EmergencyRequest) {
    return this.http.post<EmergencyRequest>(this.emRequestUrl, emRequest);
  }

  public update(emRequest: EmergencyRequest) {
    return this.http.put<User>(this.emRequestUrl, emRequest);
  }

  public findEmRequest(emRequestID: number) {
    return this.http.get<EmergencyRequest>(this.emRequestUrl + "/byID/" + emRequestID.toString());
  }

  public populateImages() {
    let images = new Map<string, string>();
    images.set("HIKING", "https://img.redbull.com/images/c_limit,w_1500,h_1000,f_auto,q_auto/redbullcom/2018/03/05/acaf2999-8c5a-477f-81bd-4095d708afb9/hike-mental-fitness");
    images.set("FIGHT", "https://thumbs.dreamstime.com/b/fight-football-game-crowd-angry-man-hitting-another-spectator-soccer-match-audience-violent-argument-two-fans-109924198.jpg");
    images.set("ELDERLY", "https://assets.entrepreneur.com/content/3x2/2000/20160520162059-lend-hand-help-elderly-home-senior-patient.jpeg");
    images.set("WALKING", "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTTwqyJcWQX60wf0caJ2-fw_XEf5uQiKNSwORmHVFtZor9YS6PwRCUYd0PmentKD5QXFkA&usqp=CAU");
    return images;
  }

}
