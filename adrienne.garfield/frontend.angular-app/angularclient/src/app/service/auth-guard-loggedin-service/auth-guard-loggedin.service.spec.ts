import { TestBed, inject } from '@angular/core/testing';

import { AuthGuardLoggedinService } from './auth-guard-loggedin.service';

describe('AuthGuardLoggedinService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AuthGuardLoggedinService]
    });
  });

  it('should be created', inject([AuthGuardLoggedinService], (service: AuthGuardLoggedinService) => {
    expect(service).toBeTruthy();
  }));
});
