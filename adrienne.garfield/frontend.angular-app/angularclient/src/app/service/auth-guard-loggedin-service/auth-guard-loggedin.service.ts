import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {AuthServiceService} from "../auth-service/auth-service.service";

@Injectable()
export class AuthGuardLoggedinService implements CanActivate {

  constructor(private authService: AuthServiceService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this.authService.isAuthenticated()) {
      this.router.navigate(['/users/' + this.authService.getCredentials().id]);
      return false;
    }
    return true;
  }

}
