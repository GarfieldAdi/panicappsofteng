import {Injectable} from '@angular/core';
import {Credentials} from "../../model/credentials";
import {HttpClient, HttpParams} from "@angular/common/http";
import {AuthServiceService} from "../auth-service/auth-service.service";
import {User} from "../../model/user";
import {Message} from "../../model/message";
import {Contact} from "../../model/contact";

@Injectable()
export class MessageService {

  messageUrl: string;
  credentials: Credentials;

  constructor(private http: HttpClient, private authService: AuthServiceService) {
    this.messageUrl = 'http://localhost:8080/messages';
    this.credentials = authService.getCredentials();
  }

  public findMessagesOfUsers(messages: Message[]) {
    return this.http.post<Message[]>(this.messageUrl + '/users', messages);
  }

  public getMessagesWithUser(message: Message) {
    return this.http.post<Message[]>(this.messageUrl + "/conversation", message);
  }

  public sendMessageToUser(message: Message) {
    return this.http.post<Message[]>(this.messageUrl, message);
  }

  public deleteConversation(message: Message) {
    return this.http.request<Contact>('delete', this.messageUrl, {body: message});
  }

}
