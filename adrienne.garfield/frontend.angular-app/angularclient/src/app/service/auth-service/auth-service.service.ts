import {Injectable} from '@angular/core';
import {User} from "../../model/user";
import {HttpClient} from "@angular/common/http";
import {Credentials} from "../../model/credentials";

@Injectable()
export class AuthServiceService {

  private readonly url: string;

  constructor(private http: HttpClient) {
    this.url = 'http://localhost:8080';
  }

  public isAuthenticated(): boolean {
    const credentials = localStorage.getItem('credentials');
    if (credentials == null) {
      return false;
    }
    return true;
  }

  public getCredentials() {
    const jsonCredentials = JSON.parse(localStorage.getItem('credentials'));
    let credentials = new Credentials();
    credentials.username = jsonCredentials["username"];
    credentials.role = jsonCredentials["role"];
    credentials.id = jsonCredentials["id"];
    return credentials;
  }

  public login(user: User) {
    const loginForm = new FormData();
    loginForm.append('username', user.username);
    loginForm.append('password', user.passw);
    loginForm.append('submit', 'login');
    return this.http.post<any>('http://localhost:8080/login', loginForm);
  }

  public logout() {
    return this.http.post('http://localhost:8080/logout', {});
  }

}
