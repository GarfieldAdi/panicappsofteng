import { Injectable } from '@angular/core';
import {Credentials} from "../../model/credentials";
import {HttpClient} from "@angular/common/http";
import {AuthServiceService} from "../auth-service/auth-service.service";
import {Observable} from "rxjs";
import {EmergencyRequest} from "../../model/emergencyRequest";
import {Allergy} from "../../model/allergy";
import {EmergencyContact} from "../../model/emergencyContact";

@Injectable()
export class AllergyService {

  allergyUrl: string;
  credentials: Credentials;

  constructor(private http: HttpClient, private authService: AuthServiceService) {
    this.allergyUrl = 'http://localhost:8080/allergies';
    this.credentials = authService.getCredentials();
  }

  public findAllWithID(): Observable<Allergy[]> {
    return this.http.get<Allergy[]>(this.allergyUrl + '/' + this.credentials.id);
  }

  public delete(allergy: Allergy) {
    return this.http.request<Allergy>('delete', this.allergyUrl, {body: allergy});
  }

  public save(allergy: Allergy) {
    return this.http.post<Allergy>(this.allergyUrl, allergy);
  }

}
