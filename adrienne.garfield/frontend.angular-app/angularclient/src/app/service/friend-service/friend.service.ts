import {Injectable} from '@angular/core';
import {Observable} from "rxjs";
import {EmergencyRequest} from "../../model/emergencyRequest";
import {Contact} from "../../model/contact";
import {HttpClient} from "@angular/common/http";
import {Credentials} from "../../model/credentials";
import {AuthServiceService} from "../auth-service/auth-service.service";
import {User} from "../../model/user";

@Injectable()
export class FriendService {

  friendUrl: string;
  credentials: Credentials;

  constructor(private http: HttpClient, private authService: AuthServiceService) {
    this.friendUrl = 'http://localhost:8080/contacts';
    this.credentials = authService.getCredentials();
  }

  public findAllFriends(): Observable<User[]> {
    return this.http.get<User[]>(this.friendUrl + '/friends/' + this.credentials.id);
  }

  public findAllRequests(): Observable<User[]> {
    return this.http.get<User[]>(this.friendUrl + '/requests/' + this.credentials.id);
  }

  public acceptFriend(contactID: number) {
    let contact = new Contact();
    contact.user1ID = contactID
    contact.user2ID = this.credentials.id;
    return this.http.put<Contact>(this.friendUrl + "/accepted", contact);
  }

  public refuseFriend(contactID: number) {
    let contact = new Contact();
    contact.user1ID = contactID
    contact.user2ID = this.credentials.id;
    return this.http.put<Contact>(this.friendUrl + "/refused", contact);
  }

  public deleteFriend(contactID: number) {
    let contact = new Contact();
    contact.user1ID = this.credentials.id;
    contact.user2ID = contactID;
    return this.http.request<Contact>('delete', this.friendUrl, {body: contact});
  }

  public addFriend(contactID: number) {
    let contact = new Contact();
    contact.user1ID = contactID;
    contact.user2ID = this.credentials.id;
    return this.http.post<Contact>(this.friendUrl, contact);
  }

}
