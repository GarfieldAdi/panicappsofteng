import {Injectable} from '@angular/core';
import {Credentials} from "../../model/credentials";
import {HttpClient} from "@angular/common/http";
import {AuthServiceService} from "../auth-service/auth-service.service";
import {Observable} from "rxjs";
import {Sickness} from "../../model/sickness";

@Injectable()
export class SicknessService {

  sicknessUrl: string;
  credentials: Credentials;

  constructor(private http: HttpClient, private authService: AuthServiceService) {
    this.sicknessUrl = 'http://localhost:8080/sicknesses';
    this.credentials = authService.getCredentials();
  }

  public findAllWithID(): Observable<Sickness[]> {
    return this.http.get<Sickness[]>(this.sicknessUrl + '/' + this.credentials.id);
  }

  public delete(sickness: Sickness) {
    return this.http.request<Sickness>('delete', this.sicknessUrl, {body: sickness});
  }

  public save(sickness: Sickness) {
    return this.http.post<Sickness>(this.sicknessUrl, sickness);
  }

}
