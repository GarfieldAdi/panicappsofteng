import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from "@angular/router";
import {Observable} from "rxjs";
import {AuthServiceService} from "../auth-service/auth-service.service";

@Injectable()
export class RoleGuardService implements CanActivate {

  constructor(private authService: AuthServiceService, private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot): boolean {
    if (!this.authService.isAuthenticated()) {
      this.router.navigate(['/login']);
      return false;
    }
    const expectedRole = route.data.expectedRole;
    const role = this.authService.getCredentials().role;

    if (role.indexOf(expectedRole) < 0) {
      this.router.navigate(['/']);
      return false;
    }
    return true;
  }

}
