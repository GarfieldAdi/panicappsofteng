import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {User} from '../../model/user'
import 'rxjs/Rx';
import {Observable} from "rxjs";
import {PasswordChange} from "../../model/passwordChange";
import {GeoLocation} from "../../model/geoLocation";
import {EmergencyRequest} from "../../model/emergencyRequest";

@Injectable()
export class UserServiceService {

  private readonly usersUrl: string;

  constructor(private http: HttpClient) {
    this.usersUrl = 'http://localhost:8080/users';
  }

  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl);
  }

  public findUser(userID: number) {
    return this.http.get<User>(this.usersUrl + "/" + userID.toString());
  }

  public findUserByUsername(username: string) {
    return this.http.get<User>(this.usersUrl + "/username/" + username);
  }

  public findLocation() {
    return this.http.get<GeoLocation>(this.usersUrl + "/geoLocation");
  }

  public save(user: User, file: File) {
    let formData = new FormData();
    const userBlob = new Blob([JSON.stringify(user)], {type: "application/json"});
    formData.append('user', userBlob);
    if (file) {
      formData.append('file', file);
    }
    return this.http.post<User>(this.usersUrl, formData);
  }

  public update(user: User, file: File) {
    let formData = new FormData();
    const userBlob = new Blob([JSON.stringify(user)], {type: "application/json"});
    formData.append('user', userBlob);
    if (file) {
      formData.append('file', file);
    }
    return this.http.put<User>(this.usersUrl, formData);
  }

  public delete(user: User) {
    return this.http.request<User>('delete', this.usersUrl, {body: user});
  }

  public generateToken(user: User) {
    return this.http.post<User>(this.usersUrl + '/generateToken', user);
  }

  public resetPassword(passwordChange: PasswordChange) {
    return this.http.put<PasswordChange>(this.usersUrl + '/resetPassword', passwordChange);
  }

  public acceptNotification(emRequestID: number) {
    let emRequest = new EmergencyRequest();
    emRequest.emRequestID = emRequestID;
    return this.http.put<EmergencyRequest>(this.usersUrl + '/acceptNotification', emRequest);
  }

}
