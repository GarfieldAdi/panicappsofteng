import { Pipe, PipeTransform } from '@angular/core';
import {User} from "../../model/user";

@Pipe({
  name: 'friendsFilter'
})
export class FriendsPipe implements PipeTransform {

  transform(value: User[], input: string): any {
    if (input) {
      return value.filter(val => val.username.indexOf(input) >= 0);
    } else {
      return value;
    }
  }

}
