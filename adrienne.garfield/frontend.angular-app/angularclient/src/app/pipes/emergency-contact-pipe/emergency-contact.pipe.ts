import {Pipe, PipeTransform} from '@angular/core';
import {EmergencyContact} from "../../model/emergencyContact";

@Pipe({
  name: 'emergencyContactFilter'
})
export class EmergencyContactPipe implements PipeTransform {

  transform(value: EmergencyContact[], input: string): any {
    if (input) {
      return value.filter(val => val.nameContact.indexOf(input) >= 0);
    } else {
      return value;
    }
  }

}
