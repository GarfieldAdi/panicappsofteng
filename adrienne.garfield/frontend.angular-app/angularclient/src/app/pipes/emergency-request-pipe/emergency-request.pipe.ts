import {Pipe, PipeTransform} from '@angular/core';
import {EmergencyRequest} from "../../model/emergencyRequest";

@Pipe({
  name: 'emergencyRequestFilter'
})
export class EmergencyRequestPipe implements PipeTransform {

  transform(value: EmergencyRequest[], input: string): any {
    if (input) {
      return value.filter(val => val.emType.indexOf(input.toUpperCase()) >= 0);
    } else {
      return value;
    }
  }

}
