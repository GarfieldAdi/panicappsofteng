import {Component, OnInit} from '@angular/core';
import {Allergy} from "../../model/allergy";
import {AllergyService} from "../../service/allergy-service/allergy.service";
import {Credentials} from "../../model/credentials";
import {AuthServiceService} from "../../service/auth-service/auth-service.service";
import {Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-allergy-form',
  templateUrl: './allergy-form.component.html',
  styleUrls: ['./allergy-form.component.css']
})
export class AllergyFormComponent implements OnInit {

  allergy: Allergy
  credentials: Credentials
  severenesses: number[];
  errorMsg: string;
  @Output() newAllergyEvent = new EventEmitter<Allergy>();

  constructor(private allergyService: AllergyService, authService: AuthServiceService) {
    this.allergy = new Allergy();
    this.credentials = authService.getCredentials();
    this.allergy.userID = this.credentials.id;
    this.allergy.severeness = 1;
    this.severenesses = [1, 2, 3, 4, 5];
    this.errorMsg = '';
  }

  ngOnInit() {
  }

  public onAdd() {
    this.allergyService.save(this.allergy).subscribe(
      (data) => {
        this.newAllergyEvent.emit(data);
      },
      () => {
        this.errorMsg = 'Could not add allergy!'
      }
    )
  }

}
