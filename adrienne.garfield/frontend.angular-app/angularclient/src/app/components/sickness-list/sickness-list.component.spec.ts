import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SicknessListComponent } from './sickness-list.component';

describe('SicknessListComponent', () => {
  let component: SicknessListComponent;
  let fixture: ComponentFixture<SicknessListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SicknessListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SicknessListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
