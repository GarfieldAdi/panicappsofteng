import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Sickness} from "../../model/sickness";
import {SicknessService} from "../../service/sickness-service/sickness.service";

@Component({
  selector: 'app-sickness-list',
  templateUrl: './sickness-list.component.html',
  styleUrls: ['./sickness-list.component.css']
})
export class SicknessListComponent implements OnInit {

  sicknesses: Sickness[];
  errorMsg: string;
  showAddSickness: boolean;

  constructor(private sicknessService: SicknessService) {
    this.errorMsg = '';
    this.showAddSickness = false;
  }

  ngOnInit() {
    this.sicknessService.findAllWithID().subscribe(data => {
        this.sicknesses = data;
      },
      () => {
        this.errorMsg = 'Could not find sicknesses!';
      })
  }

  public onDelete(sickness: Sickness) {
    this.sicknessService.delete(sickness).subscribe((res) => {
        this.sicknesses = this.sicknesses.filter(sickness => sickness.sicknessID !== res.sicknessID);
      },
      () => {
        this.errorMsg = 'Could not delete request!';
      });
  }

  public onShowAddSickness() {
    this.showAddSickness = true;
  }

  public onHideAddSickness() {
    this.showAddSickness = false;
  }

  public addSickness(sickness: Sickness) {
    this.sicknesses.push(sickness);
    this.showAddSickness = false;
  }

}
