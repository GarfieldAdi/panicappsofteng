import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserServiceService} from '../../service/user-service/user-service.service';
import {User} from '../../model/user';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.css']
})
export class UserFormComponent implements OnInit {

  user: User;
  errorMsg: string;
  file: File;
  url: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserServiceService) {
    this.user = new User();
    this.errorMsg = '';
  }

  onRegistration() {
    this.userService.save(this.user, this.file).subscribe(() => this.gotoUserList(),
      (err) => {
        console.log(err);
        this.errorMsg = 'Could not registrate!'
      });
  }

  onImageUploaded(event) {
    let reader = new FileReader();
    this.file = event.target.files[0];
    reader.readAsDataURL(this.file);
    reader.onload = () => {
      this.url = reader.result;
    };
  }

  gotoUserList() {
    this.router.navigate(['/users']);
  }

  ngOnInit(): void {
  }

}
