import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyRequestListComponent } from './emergency-request-list.component';

describe('EmergencyRequestListComponent', () => {
  let component: EmergencyRequestListComponent;
  let fixture: ComponentFixture<EmergencyRequestListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmergencyRequestListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyRequestListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
