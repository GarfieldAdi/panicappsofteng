import {Component, OnInit} from '@angular/core';
import {EmergencyRequest} from "../../model/emergencyRequest";
import {EmergencyRequestService} from "../../service/emergency-request-service/emergency-request.service";
import {EmergencyRequestPipe} from "../../pipes/emergency-request-pipe/emergency-request.pipe";

@Component({
  selector: 'app-emergency-request-list',
  templateUrl: './emergency-request-list.component.html',
  styleUrls: ['./emergency-request-list.component.css']
})
export class EmergencyRequestListComponent implements OnInit {

  errorMsg: string;
  emRequests: EmergencyRequest[];
  images: Map<string, string>;
  emergencyType: string;

  constructor(private emRequestService: EmergencyRequestService, private emRequestPipe: EmergencyRequestPipe) {
    this.errorMsg = '';
    this.images = emRequestService.populateImages();
    this.emergencyType = '';
  }

  ngOnInit() {
    this.emRequestService.findAllWithID().subscribe(data => {
        this.emRequests = data;
      },
      (err) => {
        this.errorMsg = 'Could not find requests!';
        console.log(err);
      })
  }

  public onDelete(emRequest: EmergencyRequest) {
    this.emRequestService.delete(emRequest).subscribe((res) => {
        this.emRequests = this.emRequests.filter(emRequest => emRequest.emRequestID !== res.emRequestID);
      },
      () => {
        this.errorMsg = 'Could not delete request!';
      });
  }

}
