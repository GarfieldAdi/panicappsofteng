import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthServiceService} from "../../service/auth-service/auth-service.service";
import {Credentials} from "../../model/credentials";

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  credentials: Credentials;

  constructor(private authService: AuthServiceService, private router: Router,) {
    this.credentials = JSON.parse(localStorage.getItem('credentials'));
  }

  ngOnInit() {

  }

  public profile(id): void {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
      this.router.navigate(['/users/' + id]));
  }

  public logout() {
    this.authService.logout().subscribe(() => {
        localStorage.clear();
        this.router.navigate(['/login']);
      }
    );
  }

}
