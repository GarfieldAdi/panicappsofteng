import {Component, OnInit} from '@angular/core';
import {Sickness} from "../../model/sickness";
import {SicknessService} from "../../service/sickness-service/sickness.service";
import {Credentials} from "../../model/credentials";
import {AuthServiceService} from "../../service/auth-service/auth-service.service";
import {Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-sickness-form',
  templateUrl: './sickness-form.component.html',
  styleUrls: ['./sickness-form.component.css']
})
export class SicknessFormComponent implements OnInit {

  sickness: Sickness
  credentials: Credentials
  severenesses: number[];
  errorMsg: string;
  @Output() newSicknessEvent = new EventEmitter<Sickness>();

  constructor(private sicknessService: SicknessService, authService: AuthServiceService) {
    this.sickness = new Sickness();
    this.credentials = authService.getCredentials();
    this.sickness.userID = this.credentials.id;
    this.sickness.severeness = 1;
    this.severenesses = [1, 2, 3, 4, 5];
    this.errorMsg = '';
  }

  ngOnInit() {
  }

  public onAdd() {
    this.sicknessService.save(this.sickness).subscribe(
      (data) => {
        this.newSicknessEvent.emit(data);
      },
      () => {
        this.errorMsg = 'Could not add sickness!'
      }
    )
  }

}
