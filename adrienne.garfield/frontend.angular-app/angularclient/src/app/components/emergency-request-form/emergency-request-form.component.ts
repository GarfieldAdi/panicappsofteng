import {Component, OnInit} from '@angular/core';
import {EmergencyRequest} from "../../model/emergencyRequest";
import {Credentials} from "../../model/credentials";
import {ActivatedRoute, Router} from "@angular/router";
import {EmergencyRequestService} from "../../service/emergency-request-service/emergency-request.service";
import {DatePipe} from "@angular/common";

@Component({
  selector: 'app-emergency-request-form',
  templateUrl: './emergency-request-form.component.html',
  styleUrls: ['./emergency-request-form.component.css']
})

export class EmergencyRequestFormComponent implements OnInit {

  emRequest: EmergencyRequest;
  errorMsg: string;
  credentials: Credentials;
  emTypes: string[];
  hourInt: number;
  minInt: number;
  dateInt: string;
  images: Map<string, string>;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private emRequestService: EmergencyRequestService,
              private datePipe: DatePipe) {
    this.errorMsg = '';
    this.credentials = JSON.parse(localStorage.getItem('credentials'));
    this.emTypes = ['HIKING', 'WALKING', 'FIGHT', 'ELDERLY'];
    this.dateInt = new Date().toISOString().split('T')[0];

    this.emRequest = new EmergencyRequest();
    this.emRequest.emType = 'HIKING';
    this.emRequest.userID = this.credentials.id;
    this.emRequest.startDate = new Date();

    this.images = emRequestService.populateImages();
  }

  public onAdd() {
    if (this.hourInt < 0 || this.hourInt > 23 || this.minInt < 1 || this.minInt > 59) {
      this.errorMsg = "Not valid end date!";
    }else {
      this.emRequest.endDate = new Date(this.dateInt + ' ' + this.hourInt + ":" + this.minInt + ":00");
      if (this.emRequest.startDate.getTime() - this.emRequest.endDate.getTime() >= 0) {
        this.errorMsg = 'Could not add emergency! The date should not be in the past!';
      } else {
        console.log('xxx');
        this.emRequestService.save(this.emRequest).subscribe(
          () => {
            this.requestList();
          },
          (err) => {
            console.log(err);
            // this.errorMsg = 'Could not add request!';
            this.errorMsg = 'xxx';
          }
        )
      }
    }
  }

  public requestList() {
    this.router.navigate(["/emergencyRequests"]);
  }

  ngOnInit() {
  }

}
