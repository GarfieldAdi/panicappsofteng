import { Component, OnInit } from '@angular/core';
import {Credentials} from "../../model/credentials";
import {AuthServiceService} from "../../service/auth-service/auth-service.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  credentials: Credentials;

  constructor(authService: AuthServiceService) {
    this.credentials = authService.getCredentials();
  }

  ngOnInit() {
  }

}
