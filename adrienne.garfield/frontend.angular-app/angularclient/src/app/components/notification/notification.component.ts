import {Component, OnDestroy, OnInit} from '@angular/core';
import {WebSocketService} from "../../service/web-socket-service/web-socket.service";
import {Notification} from "../../model/notification";
import {UserServiceService} from "../../service/user-service/user-service.service";

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.css']
})
export class NotificationComponent implements OnInit, OnDestroy {

  public notification = new Notification();
  public hiddenAlert: boolean;
  public errorMsg: string;
  public audio: any;

  constructor(private webSocketService: WebSocketService, private userService: UserServiceService) {

    let stompClient = this.webSocketService.connect();
    this.hiddenAlert = true;
    this.errorMsg = '';
    this.audio = new Audio();
    this.audio.src = "../../../assets/audio/alert.wav";
    this.audio.load();
    stompClient.connect({}, () => {

      stompClient.subscribe('/topic/notification', notifications => {
        this.notification = JSON.parse(notifications.body);
        this.hiddenAlert = false;
        this.audio.loop = true;
        this.audio.play();
      })
    });
  }

  public alertAccept(emRequestID: number) {
    this.userService.acceptNotification(emRequestID).subscribe(
      () => {
        this.audio.pause();
        this.hiddenAlert = true;
        this.errorMsg = '';
      },
      () => {
        this.audio.pause();
        this.errorMsg = 'Could not accept alert!';
      }
    )
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.audio.pause();
  }

}
