import {Component, OnInit} from '@angular/core';
import {User} from "../../model/user";
import {FriendService} from "../../service/friend-service/friend.service";
import {UserServiceService} from "../../service/user-service/user-service.service";
import {ImageService} from "../../service/image-service/image.service";

@Component({
  selector: 'app-friend-list',
  templateUrl: './friend-list.component.html',
  styleUrls: ['./friend-list.component.css']
})
export class FriendListComponent implements OnInit {

  friends: User[];
  errorMessage: string;
  requests: User[];
  username: string;
  usernameAdd: string;
  imagesOfFriends: any[];
  imagesOfRequests: any[];

  constructor(private friendService: FriendService, private userService: UserServiceService,
              private imageService: ImageService) {
    this.errorMessage = '';
    this.requests = [];
    this.username = '';
    this.usernameAdd = '';
    this.friends = [];
    this.requests = [];
    this.imagesOfFriends = [];
    this.imagesOfRequests = [];
  }

  ngOnInit() {
    this.friendService.findAllFriends().subscribe(
      (data) => {
        this.friends = data;
        this.friendService.findAllRequests().subscribe(
          (data) => {
            this.requests = data;
            this.getImagesOfFriends();
            this.getImagesOfRequests();
          }, (err) => {
            this.errorMessage = 'Could not load requests!';
          }
        )
      },
      (err) => {
        this.errorMessage = 'Could not load friends!';
      }
    );
    if (this.friends.length === 0) {
      this.getImagesOfRequests();
    }
  }

  public getImagesOfFriends() {
    this.imageService.getAllImagesOfFriends(this.friends).subscribe(
      data => {
        this.imagesOfFriends = this.imageService.getFilesFromEncoded(data);
      },
      err => {
        this.errorMessage = 'Could not get images of friends!';
      }
    )
  }

  public getImagesOfRequests() {
    this.imageService.getAllImagesOfFriends(this.requests).subscribe(
      data => {
        this.imagesOfRequests = this.imageService.getFilesFromEncoded(data);
      },
      err => {
        this.errorMessage = 'Could not get images of requests!';
      }
    )
  }

  public acceptFriend(contact: User) {
    this.friendService.acceptFriend(contact.userID).subscribe(
      () => {
        this.removeFromListAndImages(contact, this.requests, this.imagesOfRequests);
        this.addToListAndImages(contact, this.friends, this.imagesOfFriends);
      },
      () => {
        this.errorMessage = 'Could not accept friend!';
      }
    );
  }

  public refuseFriend(contact: User) {
    this.friendService.refuseFriend(contact.userID).subscribe(
      () => {
        this.removeFromListAndImages(contact, this.requests, this.imagesOfRequests);
      },
      () => {
        this.errorMessage = 'Could not refuse friend!';
      }
    );
  }

  public onAdd() {
    this.userService.findUserByUsername(this.usernameAdd).subscribe(
      (data) => {
        this.friendService.addFriend(data.userID).subscribe(
          () => {
            this.errorMessage = 'Request sent!';
          },
          () => {
            this.errorMessage = 'Could not add friend!';
          }
        )
      },
      (err) => {
        this.errorMessage = 'Could not find user!';
      }
    )
  }

  public onDelete(contact: User) {
    this.friendService.deleteFriend(contact.userID).subscribe(
      () => {
        this.removeFromListAndImages(contact, this.friends, this.imagesOfFriends);
      },
      () => {
        this.errorMessage = 'Could not delete friend!';
      }
    )
  }

  public addToListAndImages(contact: User, list: User[], images: any[]){
    this.imageService.getFile(contact.fileName).subscribe(
      data => {
        let image = this.imageService.getFileFromEncoded(data);
        list.push(contact);
        images.push(image);
      },
      err => {
        this.errorMessage = 'Could not get image of user!';
      }
    )
  }

  public removeFromListAndImages(contact: User, list: User[], images: any[]) {
    let indexOfDeleted = list.findIndex(friend => friend.userID === contact.userID);
    list.splice(indexOfDeleted, 1);
    images.splice(indexOfDeleted, 1);
  }

}
