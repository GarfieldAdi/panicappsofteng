import {Component, OnInit} from '@angular/core';
import {GeoLocation} from "../../model/geoLocation";
import {UserServiceService} from "../../service/user-service/user-service.service";
import {ViewChild} from '@angular/core';
import {} from '@types/googlemaps';

@Component({
  selector: 'app-geolocation',
  templateUrl: './geolocation.component.html',
  styleUrls: ['./geolocation.component.css']
})
export class GeolocationComponent implements OnInit {

  geoLocation: GeoLocation;
  errorMsg: string;
  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;
  marker: google.maps.Marker;

  constructor(private userService: UserServiceService) {
    this.geoLocation = new GeoLocation();
    this.errorMsg = '';
  }

  ngOnInit() {
    this.userService.findLocation().subscribe(
      (data) => {
        this.geoLocation = data;
        let latitude = parseFloat(data.latitude);
        let longitude = parseFloat(data.longitude);
        let mapDetails = {
          center: new google.maps.LatLng({lat: latitude, lng: longitude}),
          zoom: 13
        };
        this.map = new google.maps.Map(this.gmapElement.nativeElement, mapDetails);
        new google.maps.Marker({
          position: new google.maps.LatLng({lat: latitude, lng: longitude}),
          map: this.map,
          title: 'My position',
        })
        this.marker = new google.maps.Marker(this.gmapElement.nativeElement,)
      },
      () => {
        this.errorMsg = 'Could not find user location!';
      }
    )
  }

}
