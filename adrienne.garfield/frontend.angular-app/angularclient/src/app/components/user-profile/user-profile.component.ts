import {AfterContentInit, AfterViewInit, Component, OnInit} from '@angular/core';
import {User} from "../../model/user";
import {UserServiceService} from '../../service/user-service/user-service.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Credentials} from "../../model/credentials";
import {AuthServiceService} from "../../service/auth-service/auth-service.service";
import {ImageService} from "../../service/image-service/image.service";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit, AfterContentInit {

  user: User;
  id: number;
  credentials: Credentials;
  isDataLoaded: boolean;
  onEdit: boolean;
  errorMessage: string;
  currentPassw: string;
  allergyShow: boolean;
  retrievedImage: any;
  image: any;
  file: File;

  constructor(private userService: UserServiceService, private authService: AuthServiceService,
              private imageService: ImageService, private routeParams: ActivatedRoute,
              private router: Router, private sanitizer: DomSanitizer) {
    this.credentials = authService.getCredentials();
    this.isDataLoaded = false;
    this.onEdit = true;
    this.id = routeParams.snapshot.params['id'];
    this.errorMessage = '';
    this.allergyShow = true;
  }

  ngOnInit() {
    this.userService.findUser(this.id).subscribe(data => {
        this.user = data;
        this.isDataLoaded = true;
        this.imageService.getFile(this.user.fileName).subscribe(
          data => {
            this.image = this.imageService.getFileFromEncoded(data);
          },
          err => {
            console.log(err);
          }
        )
      },
      () => {
        this.errorMessage = 'Could not find user!';
      });
  }

  ngAfterContentInit() {
    if (this.isDataLoaded) {

    }
  }

  onImageUploaded(event) {
    let reader = new FileReader();
    this.file = event.target.files[0];
    reader.readAsDataURL(this.file);
    reader.onload = () => {
      this.image = reader.result;
    };
  }

  onDelete(user: User) {
    this.userService.delete(user).subscribe(() => {
        if (user.userID == this.credentials.id) {
          localStorage.clear();
          this.router.navigate(['/login']);
        } else {
          this.router.navigate(['/users']);
        }
      },
      () => {
        this.errorMessage = 'Could not delete user!';
      });
  }

  onUpdate() {
    this.userService.findUser(this.id).subscribe(data => {
        if (data.passw !== this.currentPassw) {
          this.errorMessage = 'You entered a wrong password!';
          this.ngOnInit();
        } else {
          this.errorMessage = '';
          this.userService.update(this.user, this.file).subscribe();
        }
        this.currentPassw = '';
      },
      () => {
        this.errorMessage = 'Could not update user!';
      });
  }

  onEditClick() {
    this.onEdit = !this.onEdit;
  }

  onAllergyClick() {
    this.allergyShow = true;
  }

  onSicknessClick() {
    this.allergyShow = false;
  }

}
