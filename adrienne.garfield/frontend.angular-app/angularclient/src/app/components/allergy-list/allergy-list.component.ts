import {Component, OnInit} from '@angular/core';
import {Allergy} from "../../model/allergy";
import {AllergyService} from "../../service/allergy-service/allergy.service";

@Component({
  selector: 'app-allergy-list',
  templateUrl: './allergy-list.component.html',
  styleUrls: ['./allergy-list.component.css']
})
export class AllergyListComponent implements OnInit {

  allergies: Allergy[];
  errorMsg: string;
  showAddAllergy: boolean;

  constructor(private allergyService: AllergyService) {
    this.errorMsg = '';
    this.showAddAllergy = false;
  }

  ngOnInit() {
    this.allergyService.findAllWithID().subscribe(data => {
        this.allergies = data;
      },
      () => {
        this.errorMsg = 'Could not find allergies!';
      })
  }

  public onDelete(allergy: Allergy) {
    this.allergyService.delete(allergy).subscribe((res) => {
        this.allergies = this.allergies.filter(allergy => allergy.allergyID !== res.allergyID);
      },
      () => {
        this.errorMsg = 'Could not delete request!';
      });
  }

  public onShowAddAllergy() {
    this.showAddAllergy = true;
  }

  public onHideAddAllergy() {
    this.showAddAllergy = false;
  }

  public addAllergy(allergy: Allergy) {
    this.allergies.push(allergy);
    this.showAddAllergy = false;
  }

}
