import {AfterContentInit, AfterViewChecked, AfterViewInit, Component, OnInit} from '@angular/core';
import {User} from "../../model/user";
import {Message} from "../../model/message";
import {FriendService} from "../../service/friend-service/friend.service";
import {MessageService} from "../../service/message-service/message.service";
import {Credentials} from "../../model/credentials";
import {AuthServiceService} from "../../service/auth-service/auth-service.service";
import {UserServiceService} from "../../service/user-service/user-service.service";
import {forkJoin} from "rxjs/observable/forkJoin";
import {WebSocketService} from "../../service/web-socket-service/web-socket.service";
import {ImageService} from "../../service/image-service/image.service";

@Component({
  selector: 'app-friend-chat',
  templateUrl: './friend-chat.component.html',
  styleUrls: ['./friend-chat.component.css']
})
export class FriendChatComponent implements OnInit, AfterViewChecked {

  chatPartners: User[];
  chatPartnersImages: any[];
  messages: Message[];
  errorMessage: string;
  credentials: Credentials;
  loadedMessages: User;
  checked: boolean;
  message: string;
  messageSocket: Message;
  username: string;
  convClicked: boolean;
  users: User[];
  usersImages: any[];
  usernameNewConv: string;

  constructor(private friendService: FriendService, private messageService: MessageService,
              private authService: AuthServiceService, private userService: UserServiceService,
              private webSocketService: WebSocketService, private imageService: ImageService) {
    this.errorMessage = '';
    this.credentials = authService.getCredentials();
    this.checked = false;
    this.username = '';
    this.usernameNewConv = '';
    this.convClicked = true;
    this.users = [];
    this.chatPartners = [];
    this.chatPartnersImages = [];
    this.usersImages = [];
    let stompClient = this.webSocketService.connect();
    stompClient.connect({}, () => {
      stompClient.subscribe('/topic/message/' + this.credentials.id.toString(), messages => {
        this.messageSocket = JSON.parse(messages.body);
        if (this.loadedMessages) {
          if (this.loadedMessages.userID === this.messageSocket.senderID) {
            this.messages.push(this.messageSocket);
            document.getElementById("msgFrags").scrollIntoView({behavior: "smooth", block: "end"});
          }
        }
      })
    });
  }

  ngOnInit() {
    this.initializeFriendList();
  }

  ngAfterViewChecked() {
    if (this.loadedMessages && this.checked === false) {
      this.checked = true;
      this.loadMessages(this.loadedMessages);
    }
  }

  public convClick() {
    this.convClicked = !this.convClicked;
  }

  public removeDiv(user: User) {
    this.removeFromListAndImages(user, this.users, this.usersImages);
    this.chatPartners.unshift(user);
    this.imageService.getFile(user.fileName).subscribe(
      data => {
        let image = this.imageService.getFileFromEncoded(data);
        this.chatPartnersImages.unshift(image);
      },
      err => {
        this.errorMessage = 'Could not find image!';
      }
    )
  }

  public loadDelete(user: User) {
    let convoDiv = document.getElementById("deleteConvo" + user.username);
    convoDiv.hidden = !convoDiv.hidden;
  }

  public deleteConversation(user: User) {
    let message = new Message();
    message.user1ID = this.credentials.id;
    message.user2ID = user.userID;
    this.messageService.deleteConversation(message).subscribe(
      () => {
        this.removeFromListAndImages(user, this.chatPartners, this.chatPartnersImages);
        this.addToListAndImages(user, this.users, this.usersImages);
        if (this.chatPartners.length > 0) {
          this.loadMessages(this.chatPartners[0]);
        }
      },
      () => {
        this.errorMessage = 'Could not delete conversation!';
      }
    )
  }

  public addToListAndImages(contact: User, list: User[], images: any[]) {
    this.imageService.getFile(contact.fileName).subscribe(
      data => {
        let image = this.imageService.getFileFromEncoded(data);
        list.push(contact);
        images.push(image);
      },
      err => {
        this.errorMessage = 'Could not get image of user!';
      }
    )
  }

  public removeFromListAndImages(chatPartner: User, list: User[], images: any[]) {
    let indexOfDeleted = list.findIndex(friend => friend.userID === chatPartner.userID);
    list.splice(indexOfDeleted, 1);
    images.splice(indexOfDeleted, 1);
  }

  public loadChat() {
    let message = new Message();
    message.user1ID = this.credentials.id;
    message.user2ID = this.loadedMessages.userID;
    this.messageService.getMessagesWithUser(message).subscribe(
      (data) => {
        this.messages = data;
        this.messages.sort((msg1, msg2) => {
          if (msg1.sentDate < msg2.sentDate) {
            return -1;
          }
          if (msg1.sentDate > msg2.sentDate) {
            return 1;
          }
          return 0;
        });
      },
      () => {
        this.errorMessage = 'Could not get messages!';
      }
    )
  }

  public loadFirstMessage() {
    if (this.chatPartners.length > 0) {
      this.loadMessages(this.chatPartners[0]);
    }
  }

  public loadMessages(user: User) {
    let container = document.getElementById('chatPartner' + user.username);
    if (container) {
      if (this.loadedMessages) {
        document.getElementById('chatPartner' + this.loadedMessages.username).style.background = "#fff7de";
      }
      container.style.background = "#ffea8f";
      this.loadedMessages = user;
      this.loadChat();
    }
  }

  public sendMessage() {
    let message = new Message();
    message.senderID = this.credentials.id;
    message.recipientID = this.loadedMessages.userID;
    message.message = this.message;
    message.sentDate = new Date();
    this.messageService.sendMessageToUser(message).subscribe(
      () => {
        this.messages.push(message);
        this.message = '';
        document.getElementById("msgFrags").scrollIntoView({behavior: "smooth", block: "end"});
      },
      () => {
        this.errorMessage = 'Could not send message';
      }
    )
  }

  public initializeFriendList() {
    this.friendService.findAllFriends().subscribe(
      (data) => {
        let messages = [];
        data.forEach(user => {
          let message = new Message();
          message.recipientID = user.userID;
          message.senderID = this.credentials.id;
          messages.push(message);
        })
        this.messageService.findMessagesOfUsers(messages).subscribe(
          (data) => {
            let usersObs = [];
            data.forEach(message => {
              let userID;
              if (message.recipientID !== this.credentials.id) {
                userID = message.recipientID;
              } else {
                userID = message.senderID;
              }
              usersObs.push(this.userService.findUser(userID));
            });
            if (usersObs.length === 0) {
              this.findFriendsWithoutConv();
            }
            forkJoin(usersObs).subscribe(
              (res: User[]) => {
                this.chatPartners = res;
                if (this.chatPartners.length > 0) {
                  this.loadedMessages = this.chatPartners[0];
                }
                this.getImagesOfChats();
                this.findFriendsWithoutConv();
              },
              () => {
                this.errorMessage = 'Could not get users';
              }
            )
          },
          () => {
            this.errorMessage = 'Could not load friends of user';
          }
        )
      },
      () => {
        this.errorMessage = 'Could not load friends of user';
      }
    )
  }

  public findFriendsWithoutConv() {
    this.friendService.findAllFriends().subscribe(
      (data) => {
        data.forEach(user => {
          let differs = true;
          for (let chatPartner of this.chatPartners) {
            if (user.userID === chatPartner.userID) {
              differs = false;
              break;
            }
          }
          if (differs) {
            this.users.push(user);
          }
        })
        this.getImagesOfWithoutConv();
      },
      () => {
        this.errorMessage = 'Could not get friends';
      })
  }

  public getImagesOfChats() {
    this.imageService.getAllImagesOfFriends(this.chatPartners).subscribe(
      data => {
        this.chatPartnersImages = this.imageService.getFilesFromEncoded(data);
      },
      err => {
        this.errorMessage = 'Could not get images of friends!';
      }
    )
  }

  public getImagesOfWithoutConv() {
    this.imageService.getAllImagesOfFriends(this.users).subscribe(
      data => {
        this.usersImages = this.imageService.getFilesFromEncoded(data);
      },
      err => {
        this.errorMessage = 'Could not get images of requests!';
      }
    )
  }

}
