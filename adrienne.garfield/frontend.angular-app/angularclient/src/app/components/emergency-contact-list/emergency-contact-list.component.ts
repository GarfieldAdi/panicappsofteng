import {Component, OnInit} from '@angular/core';
import {EmergencyContact} from "../../model/emergencyContact";
import {EmergencyContactService} from "../../service/emergency-contact-service/emergency-contact.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-emergency-contact-list',
  templateUrl: './emergency-contact-list.component.html',
  styleUrls: ['./emergency-contact-list.component.css']
})
export class EmergencyContactListComponent implements OnInit {

  emContacts: EmergencyContact[];
  errorMsg: string;
  contactName: string;

  constructor(private emService: EmergencyContactService, private router: Router) {
    this.errorMsg = '';
    this.contactName = '';
  }

  ngOnInit() {
    this.emService.findAllWithID().subscribe(data => {
        this.emContacts = data;
      },
      () => {
        this.errorMsg = 'Could not find contacts!';
      })
  }

  public clickDetails(id) {
    let details = document.getElementById("details" + id);
    details.hidden = !details.hidden;
  }

  public onDelete(emContact: EmergencyContact) {
    this.emService.delete(emContact).subscribe((res) => {
        this.emContacts = this.emContacts.filter(emContact => emContact.emContactID !== res.emContactID);
      },
      () => {
        this.errorMsg = 'Could not delete contact!';
      });
  }

  public emContactProfile(id): void {
    this.router.navigateByUrl('/', {skipLocationChange: true}).then(() =>
      this.router.navigate(['/emergencyContacts/' + id]));
  }

}
