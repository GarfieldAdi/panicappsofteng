import {Component, OnInit} from '@angular/core';
import {User} from "../../model/user";
import {Credentials} from "../../model/credentials";
import {EmergencyContact} from "../../model/emergencyContact";
import {UserServiceService} from "../../service/user-service/user-service.service";
import {AuthServiceService} from "../../service/auth-service/auth-service.service";
import {ActivatedRoute, Router} from "@angular/router";
import {EmergencyContactService} from "../../service/emergency-contact-service/emergency-contact.service";

@Component({
  selector: 'app-emergency-contact-profile',
  templateUrl: './emergency-contact-profile.component.html',
  styleUrls: ['./emergency-contact-profile.component.css']
})
export class EmergencyContactProfileComponent implements OnInit {

  emContact: EmergencyContact;
  id: number;
  credentials: Credentials;
  isDataLoaded: boolean;
  onEdit: boolean;
  errorMessage: string;
  numbers: number[];

  constructor(private emContactService: EmergencyContactService, private authService: AuthServiceService, private routeParams: ActivatedRoute, private router: Router) {
    this.credentials = authService.getCredentials();
    this.isDataLoaded = false;
    this.onEdit = true;
    this.id = routeParams.snapshot.params['id'];
    this.errorMessage = '';
    this.numbers = [1, 2, 3, 4, 5];
  }

  public onDelete(emContact: EmergencyContact) {
    this.emContactService.delete(emContact).subscribe((res) => {
        this.router.navigate(['/emergencyContacts']);
      },
      (err) => {
        this.errorMessage = 'Could not delete emergency contact!';
      });
  }

  onUpdate() {
    this.emContactService.update(this.emContact).subscribe(
      () => {
      },
      () => {
        this.errorMessage = 'Could not update user!';
      }
    );
  }

  onEditClick() {
    this.onEdit = !this.onEdit;
  }

  ngOnInit() {
    this.emContactService.findEmContact(this.id).subscribe(data => {
        this.emContact = data;
        this.isDataLoaded = true;
        this.errorMessage = '';
      },
      () => {
        this.errorMessage = 'Could not find emergency contact!';
      });
  }

}
