import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmergencyContactProfileComponent } from './emergency-contact-profile.component';

describe('EmergencyContactProfileComponent', () => {
  let component: EmergencyContactProfileComponent;
  let fixture: ComponentFixture<EmergencyContactProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmergencyContactProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmergencyContactProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
