import {Component, OnInit} from '@angular/core';
import {User} from "../../model/user";
import {UserServiceService} from "../../service/user-service/user-service.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-forgotten-password-tel-num',
  templateUrl: './forgotten-password-tel-num.component.html',
  styleUrls: ['./forgotten-password-tel-num.component.css']
})
export class ForgottenPasswordTelNumComponent implements OnInit {

  errorMsg: string;
  user: User;

  constructor(private userService: UserServiceService,
              private router: Router) {
    this.errorMsg = '';
    this.user = new User();
  }

  ngOnInit() {
  }

  public onSubmit() {
    this.userService.generateToken(this.user).subscribe(
      () => {
        this.router.navigate(['/modifyPassword']);
      },
      (err) => {
        this.errorMsg = 'Could not find mobile number!';
      }
    )
  }

}
