import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgottenPasswordTelNumComponent } from './forgotten-password-tel-num.component';

describe('ForgottenPasswordTelNumComponent', () => {
  let component: ForgottenPasswordTelNumComponent;
  let fixture: ComponentFixture<ForgottenPasswordTelNumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgottenPasswordTelNumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgottenPasswordTelNumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
