import {Component, OnInit} from '@angular/core';
import {EmergencyContact} from "../../model/emergencyContact";
import {ActivatedRoute, Router} from "@angular/router";
import {EmergencyContactService} from "../../service/emergency-contact-service/emergency-contact.service";
import {Credentials} from "../../model/credentials";

@Component({
  selector: 'app-emergency-contact-form',
  templateUrl: './emergency-contact-form.component.html',
  styleUrls: ['./emergency-contact-form.component.css']
})
export class EmergencyContactFormComponent implements OnInit {

  emContact: EmergencyContact;
  errorMsg: string;
  credentials: Credentials;
  numbers: number[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private emContactService: EmergencyContactService) {
    this.errorMsg = '';
    this.credentials = JSON.parse(localStorage.getItem('credentials'));

    this.emContact = new EmergencyContact();
    this.emContact.priority = 1;
    this.emContact.userID = this.credentials.id;

    this.numbers = [1, 2, 3, 4, 5];
  }

  ngOnInit() {

  }

  public onAdd() {
    this.emContactService.save(this.emContact).subscribe(
      () => {
        this.contactList();
      },
      () => {
        this.errorMsg = 'Could not add contact!';
      }
    )
  }

  public contactList() {
    this.router.navigate(["/emergencyContacts"]);
  }

}
