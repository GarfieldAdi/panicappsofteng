import {Component, OnInit} from '@angular/core';
import {User} from "../../model/user";
import {ActivatedRoute, Router} from "@angular/router";
import {UserServiceService} from "../../service/user-service/user-service.service";
import {AuthServiceService} from "../../service/auth-service/auth-service.service";
import {forkJoin} from "rxjs/observable/forkJoin";
import {Credentials} from "../../model/credentials";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: User;
  errorMsg: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private userService: UserServiceService,
    private authService: AuthServiceService) {
    this.user = new User();
    this.errorMsg = '';
  }

  ngOnInit() {
  }

  public onLogin() {
    this.authService.login(this.user).subscribe((resp) => {
        let credentials = new Credentials();
        credentials.username = resp["username"];
        credentials.role = resp["roles"];
        credentials.id = resp["id"];
        localStorage.setItem('credentials', JSON.stringify(credentials));
        this.router.navigate(['/']);
      },
      (err) => {
        this.errorMsg = 'The password or username is not correct!';
      });
  }

}
