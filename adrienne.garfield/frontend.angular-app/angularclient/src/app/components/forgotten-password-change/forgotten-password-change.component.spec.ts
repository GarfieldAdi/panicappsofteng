import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ForgottenPasswordChangeComponent } from './forgotten-password-change.component';

describe('ForgottenPasswordChangeComponent', () => {
  let component: ForgottenPasswordChangeComponent;
  let fixture: ComponentFixture<ForgottenPasswordChangeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ForgottenPasswordChangeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ForgottenPasswordChangeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
