import {Component, OnInit} from '@angular/core';
import {PasswordChange} from "../../model/passwordChange";
import {UserServiceService} from "../../service/user-service/user-service.service";

@Component({
  selector: 'app-forgotten-password-change',
  templateUrl: './forgotten-password-change.component.html',
  styleUrls: ['./forgotten-password-change.component.css']
})
export class ForgottenPasswordChangeComponent implements OnInit {

  errorMsg: string;
  passwChange: PasswordChange;
  confirmPassw: string;

  constructor(private userService: UserServiceService) {
    this.errorMsg = '';
    this.confirmPassw = '';
    this.passwChange = new PasswordChange();
  }

  ngOnInit() {
  }

  public onSubmit() {
    if (this.confirmPassw !== this.passwChange.password) {
      this.errorMsg = 'The two passwords are not the same!'
    } else {
      this.userService.resetPassword(this.passwChange).subscribe(
        () => {
          this.errorMsg = 'Success';
        },
        () => {
          this.errorMsg = 'This code is incorrect or already expired!';
        }
      )
    }
  }

}
