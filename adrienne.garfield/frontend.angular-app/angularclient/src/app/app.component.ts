import {Component} from '@angular/core';
import {Credentials} from "./model/credentials";
import {AuthServiceService} from "./service/auth-service/auth-service.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

}
