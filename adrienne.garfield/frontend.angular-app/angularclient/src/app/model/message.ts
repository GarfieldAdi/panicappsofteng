export class Message {

  messageID: number;
  message: string;
  sentDate: Date;
  senderID: number;
  recipientID: number;
  user1ID: number;
  user2ID: number;

}
