export class Sickness {

  sicknessID: number;
  sicknessName: string;
  severeness: number;
  comments: string;
  userID: number;

}
