export class EmergencyRequest {
  emRequestID: number;
  startDate: Date;
  endDate: Date;
  timeInt: number;
  userID: number;
  emType: string;
}
