export class EmergencyContact {

  emContactID: number;
  nameContact: string;
  telNum: string;
  priority: number;
  userID: number;

}
