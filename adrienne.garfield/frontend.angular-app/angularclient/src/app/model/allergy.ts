export class Allergy {

  allergyID: number;
  allergyName: string;
  severeness: number;
  comments: string;
  userID: number;

}
