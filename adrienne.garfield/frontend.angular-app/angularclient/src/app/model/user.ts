export class User {

  userID: number;
  username: string;
  passw: string;
  telNum: string;
  birthDate: Date;
  height: number;
  weight: number;
  roleID: number;
  fileName: string;

}
