export class GeoLocation {
  ip: string;
  city: string;
  latitude: string;
  longitude: string;
}
