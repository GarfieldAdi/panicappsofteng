package com.example.panicappspringboot.service;

import com.example.panicappspringboot.dto.GeoLocationDTO;
import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Service
public class GeoLocationService {

    private final DatabaseReader databaseReader;
    private GeoLocationDTO geoLocationDTO;

    public GeoLocationService() throws IOException {
        File geoDatabase = new File("src\\main\\resources\\location\\GeoLite2-City.mmdb");
        databaseReader = new DatabaseReader.Builder(geoDatabase).build();
    }

    public String getIP(HttpServletRequest request) {
        String clientIP = request.getHeader("X-FORWARDED-FOR");
        if (clientIP == null) {
            clientIP = request.getRemoteAddr();
        }
        if (clientIP.equals("0:0:0:0:0:0:0:1")) {
            return "195.20.156.7";
        }
        return clientIP;
    }

    public GeoLocationDTO getLocation(HttpServletRequest request) {
        try {
            return getGeoLocationDTOByRequest(request);
        }
        catch (UnknownHostException e) {
            throw new EntityNotFoundException("Could not find user's location!");
        }
        catch (IOException | GeoIp2Exception e) {
            throw new RuntimeException("Could not find user's location!");
        }
    }


    public GeoLocationDTO getGeoLocationDTO() {
        return geoLocationDTO;
    }

    public void setGeoLocationDTO(HttpServletRequest request) {
        try {
            this.geoLocationDTO = getGeoLocationDTOByRequest(request);
        }
        catch (UnknownHostException e) {
            throw new EntityNotFoundException("Could not find user's location!");
        }
        catch (IOException | GeoIp2Exception e) {
            throw new RuntimeException("Could not find user's location!");
        }
    }

    private GeoLocationDTO getGeoLocationDTOByRequest(HttpServletRequest request) throws IOException, GeoIp2Exception {
        String ip = getIP(request);
        InetAddress ipAddress = InetAddress.getByName(ip);
        CityResponse response = databaseReader.city(ipAddress);

        GeoLocationDTO geoLocationDTO = new GeoLocationDTO();
        geoLocationDTO.setIpAddress(ip);
        geoLocationDTO.setCity(response.getCity().getName());
        geoLocationDTO.setLatitude(response.getLocation().getLatitude().toString());
        geoLocationDTO.setLongitude(response.getLocation().getLongitude().toString());

        return geoLocationDTO;
    }
}
