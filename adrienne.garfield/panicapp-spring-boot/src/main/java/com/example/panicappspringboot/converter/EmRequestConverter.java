package com.example.panicappspringboot.converter;

import com.example.panicappspringboot.dto.EmRequestDTO;
import com.example.panicappspringboot.model.EmergencyRequest;
import com.example.panicappspringboot.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmRequestConverter {

    public EmRequestDTO convertToDto(EmergencyRequest emRequest) {
        EmRequestDTO emRequestDTO = new EmRequestDTO();
        convertToDto(emRequest, emRequestDTO);
        return emRequestDTO;
    }

    public EmRequestDTO convertToDto(EmergencyRequest emRequest, EmRequestDTO emRequestDTO) {
        emRequestDTO.setEmRequestID(emRequest.getIdRequest());
        emRequestDTO.setEmType(emRequest.getEmType());
        emRequestDTO.setUserID(emRequest.getUser().getIdUser());
        emRequestDTO.setStartDate(emRequest.getStartDate());
        emRequestDTO.setEndDate(emRequest.getEndDate());
        emRequestDTO.setTimeInt(emRequest.getTimeInt());

        return emRequestDTO;
    }

    public EmergencyRequest convertFromDto(EmRequestDTO emRequestDTO, User user) {
        EmergencyRequest emRequest = new EmergencyRequest();
        convertFromDto(emRequestDTO, user, emRequest);
        return emRequest;
    }

    public EmergencyRequest convertFromDto(EmRequestDTO emRequestDTO, User user, EmergencyRequest emRequest) {
        emRequest.setEmType(emRequestDTO.getEmType());
        emRequest.setUser(user);
        emRequest.setStartDate(emRequestDTO.getStartDate());
        emRequest.setEndDate(emRequestDTO.getEndDate());
        emRequest.setTimeInt(emRequestDTO.getTimeInt());

        return emRequest;
    }

    public List<EmRequestDTO> convertListToDtoList(List<EmergencyRequest> emRequests) {
        List<EmRequestDTO> emRequestDTOS = new ArrayList<>();
        emRequests.forEach(emRequest -> emRequestDTOS.add(convertToDto(emRequest)));
        return emRequestDTOS;
    }

}
