package com.example.panicappspringboot.repository;

import com.example.panicappspringboot.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    List<User> findAll();

    List<User> findAllByRole_IdRole(Long idRole);

    Optional<User> getUserByUsername(String username);

    Optional<User> getUserByTelNum(String telNum);

    boolean existsUserByUsername(String username);

}
