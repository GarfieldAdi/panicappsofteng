package com.example.panicappspringboot.utils;

import org.springframework.stereotype.Component;

@Component
public class ImageUtils {

    private String imageSource = "target\\classes\\pictures\\";

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }
}
