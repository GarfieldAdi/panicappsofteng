package com.example.panicappspringboot.repository;

import com.example.panicappspringboot.model.Sickness;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface SicknessRepository extends JpaRepository<Sickness, Long> {

    List<Sickness> findAllByUser_IdUser(Long id);

}
