package com.example.panicappspringboot.service;

import com.example.panicappspringboot.controller.MessageController;
import com.example.panicappspringboot.converter.MessageConverter;
import com.example.panicappspringboot.dto.MessageDTO;
import com.example.panicappspringboot.model.Message;
import com.example.panicappspringboot.model.User;
import com.example.panicappspringboot.repository.MessageRepository;
import com.example.panicappspringboot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;

@Service
public class MessageService {

    @Autowired
    private MessageRepository messageRepo;

    @Autowired
    private MessageConverter messageConv;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private SimpMessagingTemplate template;

    public Optional<MessageDTO> getOneMessageBetweenUsers(MessageDTO messageDTO) {
        Long user1ID = messageDTO.getSenderID();
        Long user2ID = messageDTO.getRecipientID();
        Optional<Message> message = messageRepo.findFirstByUser1IdUserAndUser2IdUser(user1ID, user2ID);
        if (message.isPresent()) {
            MessageDTO messageDTOResp = messageConv.convertToDto(message.orElseThrow());
            return Optional.of(messageDTOResp);
        }
        return Optional.empty();
    }

    public List<MessageDTO> getMessagesBetweenUsers(MessageDTO messageDTO) {
        Long user1ID = messageDTO.getUser1ID();
        Long user2ID = messageDTO.getUser2ID();
        List<Message> messages = messageRepo.getMessagesByUser1IdUserAndUser2IdUser(user1ID, user2ID);
        return messageConv.convertListToDtoList(messages);
    }

    @Transactional
    public MessageDTO deleteMessage(MessageDTO messageDTO) {
        Long user1ID = messageDTO.getUser1ID();
        Long user2ID = messageDTO.getUser2ID();
        messageRepo.deleteMessagesByUser1IdUserAndUser2IdUser(user1ID, user2ID);
        return messageDTO;
    }

    @Transactional
    public MessageDTO sendMessage(MessageDTO messageDTO) {
        Long senderID = messageDTO.getSenderID();
        Long receiverID = messageDTO.getRecipientID();

        User sender = userRepo.findById(senderID).orElseThrow(() ->
                new EntityNotFoundException("User with id " + senderID + " doesn't exist. Can't insert!"));
        User receiver = userRepo.findById(receiverID).orElseThrow(() ->
                new EntityNotFoundException("User with id " + receiverID + " doesn't exist. Can't insert!"));
        Message message = messageConv.convertFromDto(messageDTO, sender, receiver, sender, receiver);
        messageRepo.save(message);
        Message message2 = messageConv.convertFromDto(messageDTO, sender, receiver, receiver, sender);
        messageRepo.save(message2);

        template.convertAndSend("/topic/message/" + receiverID, messageDTO);

        return messageDTO;
    }

}
