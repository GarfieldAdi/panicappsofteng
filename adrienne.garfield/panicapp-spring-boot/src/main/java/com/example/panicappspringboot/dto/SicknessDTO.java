package com.example.panicappspringboot.dto;

public class SicknessDTO {

    private Long sicknessID;
    private String sicknessName;
    private Integer severeness;
    private String comments;
    private Long userID;

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public Long getSicknessID() {
        return sicknessID;
    }

    public void setSicknessID(Long sicknessID) {
        this.sicknessID = sicknessID;
    }

    public String getSicknessName() {
        return sicknessName;
    }

    public void setSicknessName(String sicknessName) {
        this.sicknessName = sicknessName;
    }

    public Integer getSevereness() {
        return severeness;
    }

    public void setSevereness(Integer severeness) {
        this.severeness = severeness;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "SicknessDTO{" +
                "sicknessID=" + sicknessID +
                ", sicknessName='" + sicknessName + '\'' +
                ", severeness=" + severeness +
                ", comments='" + comments + '\'' +
                ", userID=" + userID +
                '}';
    }
}
