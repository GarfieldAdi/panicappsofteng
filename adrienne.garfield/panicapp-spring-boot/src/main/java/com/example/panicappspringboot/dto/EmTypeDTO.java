package com.example.panicappspringboot.dto;

public class EmTypeDTO {

    private Long emTypeID;
    private String emTypeName;

    public Long getEmTypeID() {
        return emTypeID;
    }

    public void setEmTypeID(Long emTypeID) {
        this.emTypeID = emTypeID;
    }

    public String getEmTypeName() {
        return emTypeName;
    }

    public void setEmTypeName(String emTypeName) {
        this.emTypeName = emTypeName;
    }

    @Override
    public String toString() {
        return "EmTypeDTO{" +
                "emTypeID=" + emTypeID +
                ", emTypeName='" + emTypeName + '\'' +
                '}';
    }
}
