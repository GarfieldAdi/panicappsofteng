package com.example.panicappspringboot.dto;

import com.example.panicappspringboot.model.EmergencyType;

import java.util.Date;


public class EmRequestDTO {

    private Long emRequestID;
    private Date startDate;
    private Date endDate;
    private Double timeInt;
    private Long userID;
    private EmergencyType emType;

    public Long getEmRequestID() {
        return emRequestID;
    }

    public void setEmRequestID(Long emRequestID) {
        this.emRequestID = emRequestID;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Double getTimeInt() {
        return timeInt;
    }

    public void setTimeInt(Double timeInt) {
        this.timeInt = timeInt;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public EmergencyType getEmType() {
        return emType;
    }

    public void setEmType(EmergencyType emType) {
        this.emType = emType;
    }

    @Override
    public String toString() {
        return "EmRequestDTO{" +
                "emRequestID=" + emRequestID +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", timeInt=" + timeInt +
                ", userID=" + userID +
                ", emType=" + emType +
                '}';
    }
}
