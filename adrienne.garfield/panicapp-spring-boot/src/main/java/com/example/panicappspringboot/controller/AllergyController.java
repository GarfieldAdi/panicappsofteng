package com.example.panicappspringboot.controller;

import com.example.panicappspringboot.dto.AllergyDTO;
import com.example.panicappspringboot.service.AllergyService;
import com.example.panicappspringboot.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/allergies")
public class AllergyController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AllergyController.class);

    @Autowired
    AllergyService allergyService;

    @Autowired
    SecurityService securityService;

    @GetMapping(path = "/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<AllergyDTO>> getAllergiesOfUser(@PathVariable Long userID) {
        LOGGER.info("Find allergies of user with id: " + userID);
        if (!securityService.authorized(userID)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        List<AllergyDTO> allergyDTOs = allergyService.findAllergiesOfUserByID(userID);
        LOGGER.info("Found successfully allergies of user with id: " + userID);
        return new ResponseEntity<>(allergyDTOs, HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AllergyDTO> addAllergy(@RequestBody AllergyDTO allergyDTO) {
        LOGGER.info("Inserts new allergy for user with id = {}", allergyDTO.getUserID());
        if (!securityService.authorized(allergyDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        AllergyDTO allergyDTOResp = allergyService.insertAllergy(allergyDTO);
        LOGGER.info("Inserted succesfully entity: {}", allergyDTOResp);
        return new ResponseEntity<>(allergyDTOResp, HttpStatus.CREATED);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AllergyDTO> deleteAllergy(@RequestBody AllergyDTO allergyDTO) {
        LOGGER.info("Deletes allergy with id = {}", allergyDTO.getAllergyID());
        if (!securityService.authorized(allergyDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        AllergyDTO allergyDTOResp = allergyService.deleteAllergyByID(allergyDTO);
        LOGGER.info("Deleted succesfully entity: {}", allergyDTOResp);
        return new ResponseEntity<>(allergyDTOResp, HttpStatus.OK);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<AllergyDTO> updateAllergy(@RequestBody AllergyDTO allergyDTO) {
        LOGGER.info("Updates allergy with id = {}", allergyDTO.getAllergyID());
        if (!securityService.authorized(allergyDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        AllergyDTO allergyDTOResp = allergyService.updateAllergy(allergyDTO);
        LOGGER.info("Updated succesfully entity: {}", allergyDTOResp);
        return new ResponseEntity<>(allergyDTOResp, HttpStatus.CREATED);
    }

}
