package com.example.panicappspringboot.repository;

import com.example.panicappspringboot.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role getRoleByUserRole(String userRole);

}
