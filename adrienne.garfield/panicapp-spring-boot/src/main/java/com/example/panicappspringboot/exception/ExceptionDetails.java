package com.example.panicappspringboot.exception;

import java.time.LocalDateTime;

public class ExceptionDetails {

    private String timestamp;
    private Integer statusCode;
    private String error;
    private String message;
    private String messageCode;
    private String path;

    public ExceptionDetails(Integer statusCode, String error, String message, String messageCode, String path) {
        this.timestamp = LocalDateTime.now().toString();
        this.statusCode = statusCode;
        this.error = error;
        this.message = message;
        this.messageCode = messageCode;
        this.path = path;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(String messageCode) {
        this.messageCode = messageCode;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public String toString() {
        return "ExceptionDetails{" +
                "timestamp='" + timestamp + '\'' +
                ", statusCode=" + statusCode +
                ", error='" + error + '\'' +
                ", message='" + message + '\'' +
                ", messageCode='" + messageCode + '\'' +
                ", path='" + path + '\'' +
                '}';
    }
}
