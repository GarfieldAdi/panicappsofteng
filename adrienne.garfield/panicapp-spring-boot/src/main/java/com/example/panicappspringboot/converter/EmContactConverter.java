package com.example.panicappspringboot.converter;

import com.example.panicappspringboot.dto.EmContactDTO;
import com.example.panicappspringboot.model.EmergencyContact;
import com.example.panicappspringboot.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class EmContactConverter {

    public EmergencyContact convertFromDTO(EmContactDTO emContactDTO, User user) {
        EmergencyContact emContact = new EmergencyContact();
        convertFromDTO(emContactDTO, user, emContact);
        return emContact;
    }

    public EmergencyContact convertFromDTO(EmContactDTO emContactDTO, User user, EmergencyContact emContact) {
        emContact.setIdContact(emContactDTO.getEmContactID());
        emContact.setNameContact(emContactDTO.getNameContact());
        emContact.setPriority(emContactDTO.getPriority());
        emContact.setTelNum(emContactDTO.getTelNum());
        emContact.setUser(user);

        return emContact;
    }

    public EmContactDTO convertToDto(EmergencyContact emContact) {
        EmContactDTO emContactDTO = new EmContactDTO();
        convertToDto(emContact, emContactDTO);
        return emContactDTO;
    }

    public EmContactDTO convertToDto(EmergencyContact emContact, EmContactDTO emContactDTO) {
        emContactDTO.setEmContactID(emContact.getIdContact());
        emContactDTO.setNameContact(emContact.getNameContact());
        emContactDTO.setPriority(emContact.getPriority());
        emContactDTO.setTelNum(emContact.getTelNum());
        emContactDTO.setUserID(emContact.getUser().getIdUser());

        return emContactDTO;
    }

    public List<EmContactDTO> convertListToDtoList(List<EmergencyContact> emContacts) {
        List<EmContactDTO> emContactDTOS = new ArrayList<>();
        emContacts.forEach(emContact -> emContactDTOS.add(convertToDto(emContact)));
        return emContactDTOS;
    }

}
