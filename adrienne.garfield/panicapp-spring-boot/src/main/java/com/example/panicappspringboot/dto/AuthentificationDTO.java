package com.example.panicappspringboot.dto;

import java.util.Arrays;

public class AuthentificationDTO {

    private String username;
    private String[] roles;
    private Long id;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String[] getRoles() {
        return roles;
    }

    public void setRoles(String[] roles) {
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "AuthentificationDTO{" +
                "username='" + username + '\'' +
                ", roles=" + Arrays.toString(roles) +
                ", id=" + id +
                '}';
    }
}
