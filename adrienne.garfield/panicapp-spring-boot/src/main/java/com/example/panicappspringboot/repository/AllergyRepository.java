package com.example.panicappspringboot.repository;

import com.example.panicappspringboot.model.Allergy;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AllergyRepository extends JpaRepository<Allergy, Long> {

    List<Allergy> findAllByUser_IdUser(Long id);

}
