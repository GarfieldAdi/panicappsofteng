package com.example.panicappspringboot.controller;

import com.example.panicappspringboot.dto.SicknessDTO;
import com.example.panicappspringboot.service.SecurityService;
import com.example.panicappspringboot.service.SicknessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/sicknesses")
public class SicknessController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SicknessController.class);

    @Autowired
    SicknessService sicknessService;

    @Autowired
    SecurityService securityService;

    @GetMapping(path = "/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<SicknessDTO>> getSicknessesOfUser(@PathVariable Long userID) {
        LOGGER.info("Find sicknesses of user with id: " + userID);
        if (!securityService.authorized(userID)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        List<SicknessDTO> sicknessDTOs = sicknessService.findSicknessesOfUserByID(userID);
        LOGGER.info("Found successfully sicknesses of user with id: " + userID);
        return new ResponseEntity<>(sicknessDTOs, HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SicknessDTO> addSickness(@RequestBody SicknessDTO sicknessDTO) {
        LOGGER.info("Inserts new sickness for user with id = {}", sicknessDTO.getUserID());
        if (!securityService.authorized(sicknessDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        SicknessDTO sicknessDTOResp = sicknessService.insertSickness(sicknessDTO);
        LOGGER.info("Inserted succesfully entity: {}", sicknessDTOResp);
        return new ResponseEntity<>(sicknessDTOResp, HttpStatus.CREATED);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SicknessDTO> deleteSickness(@RequestBody SicknessDTO sicknessDTO) {
        LOGGER.info("Deletes sickness with id = {}", sicknessDTO.getSicknessID());
        if (!securityService.authorized(sicknessDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        SicknessDTO sicknessDTOResp = sicknessService.deleteSicknessByID(sicknessDTO);
        LOGGER.info("Deleted succesfully entity: {}", sicknessDTOResp);
        return new ResponseEntity<>(sicknessDTOResp, HttpStatus.OK);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SicknessDTO> updateSickness(@RequestBody SicknessDTO sicknessDTO) {
        LOGGER.info("Updates sickness with id = {}", sicknessDTO.getSicknessID());
        if (!securityService.authorized(sicknessDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        SicknessDTO sicknessDTOResp = sicknessService.updateSickness(sicknessDTO);
        LOGGER.info("Updated succesfully entity: {}", sicknessDTOResp);
        return new ResponseEntity<>(sicknessDTOResp, HttpStatus.CREATED);
    }

}
