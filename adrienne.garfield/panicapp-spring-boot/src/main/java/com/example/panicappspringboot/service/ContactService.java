package com.example.panicappspringboot.service;

import com.example.panicappspringboot.converter.ContactConverter;
import com.example.panicappspringboot.converter.UserConverter;
import com.example.panicappspringboot.dto.ContactDTO;
import com.example.panicappspringboot.dto.EmRequestDTO;
import com.example.panicappspringboot.dto.UserDTO;
import com.example.panicappspringboot.model.Contact;
import com.example.panicappspringboot.model.ContactType;
import com.example.panicappspringboot.model.EmergencyRequest;
import com.example.panicappspringboot.model.User;
import com.example.panicappspringboot.repository.ContactRepository;
import com.example.panicappspringboot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ContactService {

    @Autowired
    private ContactRepository contactRepo;

    @Autowired
    private ContactConverter contactConv;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private UserConverter userConv;

    public List<UserDTO> findFriendsOfUser(Long userID) {
        User user1 = userRepo.findById(userID).orElseThrow(() ->
                new EntityNotFoundException("User with id " + userID + " doesn't exist. Can't insert!"));
        List<Contact> contacts = contactRepo.findAllByUser1AndContactType(user1, ContactType.FRIENDS);
        List<UserDTO> userDTOS = new ArrayList<>();
        contacts.forEach((contact -> userDTOS.add(userConv.convertToDto(contact.getUser2()))));
        return userDTOS;
    }

    public List<UserDTO> findRequestsOfUser(Long userID) {
        User user1 = userRepo.findById(userID).orElseThrow(() ->
                new EntityNotFoundException("User with id " + userID + " doesn't exist. Can't insert!"));
        List<Contact> contacts = contactRepo.findAllByUser1AndContactType(user1, ContactType.PENDING);
        List<UserDTO> userDTOS = new ArrayList<>();
        contacts.forEach((contact -> userDTOS.add(userConv.convertToDto(contact.getUser2()))));
        return userDTOS;
    }

    @Transactional
    public ContactDTO addContact(ContactDTO contactDTO) {
        Long userID1 = contactDTO.getUser1ID();
        Long userID2 = contactDTO.getUser2ID();

        User user1 = userRepo.findById(userID1).orElseThrow(() ->
                new EntityNotFoundException("User with id " + userID1 + " doesn't exist. Can't insert!"));
        User user2 = userRepo.findById(userID2).orElseThrow(() ->
                new EntityNotFoundException("User with id " + userID2 + " doesn't exist. Can't insert!"));

        if (contactRepo.existsByUser1AndUser2(user1, user2) || contactRepo.existsByUser1AndUser2(user2, user1)) {
            throw new EntityExistsException("Relationship between users already exists");
        }

        Contact contact = new Contact();
        contact.setUser1(user1);
        contact.setUser2(user2);
        contact.setContactTypeEnum(ContactType.PENDING);
        contactRepo.save(contact);

        Contact contact2 = new Contact();
        contact2.setUser1(user2);
        contact2.setUser2(user1);
        contact2.setContactTypeEnum(ContactType.REQUESTED);
        contactRepo.save(contact2);

        return contactConv.convertToDto(contact);
    }

    @Transactional
    public ContactDTO refuseContact(ContactDTO contactDTO) {
        Long userID1 = contactDTO.getUser1ID();
        Long userID2 = contactDTO.getUser2ID();

        User user1 = userRepo.findById(userID1).orElseThrow(() ->
                new EntityNotFoundException("User with id " + userID1 + " doesn't exist. Can't insert!"));
        User user2 = userRepo.findById(userID2).orElseThrow(() ->
                new EntityNotFoundException("User with id " + userID2 + " doesn't exist. Can't insert!"));


        Contact contact = contactRepo.findByUser1AndUser2(user1, user2).orElseThrow(() ->
                new EntityNotFoundException("Contact between users " + user1.getUsername() + " " + user2.getUsername() + " not found"));
        Contact contact2 = contactRepo.findByUser1AndUser2(user2, user1).orElseThrow(() ->
                new EntityNotFoundException("Contact between users " + user2.getUsername() + " " + user1.getUsername() + " not found"));

        ContactType contactType1 = contact.getContactTypeEnum();
        ContactType contactType2 = contact2.getContactTypeEnum();

        if (!contactType1.equals(ContactType.REQUESTED) && !contactType2.equals(ContactType.PENDING)) {
            throw new RuntimeException("Contact types of users are neither pending or requested");
        }

        contact.setContactTypeEnum(ContactType.REFUSED);
        contact2.setContactTypeEnum(ContactType.REFUSED);

        contactRepo.save(contact);
        contactRepo.save(contact2);

        return contactConv.convertToDto(contact);
    }

    @Transactional
    public ContactDTO acceptContact(ContactDTO contactDTO) {
        Long userID1 = contactDTO.getUser1ID();
        Long userID2 = contactDTO.getUser2ID();

        User user1 = userRepo.findById(userID1).orElseThrow(() ->
                new EntityNotFoundException("User with id " + userID1 + " doesn't exist. Can't insert!"));
        User user2 = userRepo.findById(userID2).orElseThrow(() ->
                new EntityNotFoundException("User with id " + userID2 + " doesn't exist. Can't insert!"));


        Contact contact = contactRepo.findByUser1AndUser2(user1, user2).orElseThrow(() ->
                new EntityNotFoundException("Contact between users " + user1.getUsername() + " " + user2.getUsername() + " not found"));
        Contact contact2 = contactRepo.findByUser1AndUser2(user2, user1).orElseThrow(() ->
                new EntityNotFoundException("Contact between users " + user2.getUsername() + " " + user1.getUsername() + " not found"));

        ContactType contactType1 = contact.getContactTypeEnum();
        ContactType contactType2 = contact2.getContactTypeEnum();

        if (!contactType1.equals(ContactType.REQUESTED) && !contactType2.equals(ContactType.PENDING)) {
            throw new RuntimeException("Contact types of users are neither pending or requested");
        }

        contact.setContactTypeEnum(ContactType.FRIENDS);
        contact2.setContactTypeEnum(ContactType.FRIENDS);

        contactRepo.save(contact);
        contactRepo.save(contact2);

        return contactConv.convertToDto(contact);
    }

    @Transactional
    public ContactDTO deleteContact(ContactDTO contactDTO) {
        Long userID1 = contactDTO.getUser1ID();
        Long userID2 = contactDTO.getUser2ID();

        User user1 = userRepo.findById(userID1).orElseThrow(() ->
                new EntityNotFoundException("User with id " + userID1 + " doesn't exist. Can't insert!"));
        User user2 = userRepo.findById(userID2).orElseThrow(() ->
                new EntityNotFoundException("User with id " + userID2 + " doesn't exist. Can't insert!"));


        Contact contact = contactRepo.findByUser1AndUser2(user1, user2).orElseThrow(() ->
                new EntityNotFoundException("Contact between users " + user1.getUsername() + " " + user2.getUsername() + " not found"));
        Contact contact2 = contactRepo.findByUser1AndUser2(user2, user1).orElseThrow(() ->
                new EntityNotFoundException("Contact between users " + user2.getUsername() + " " + user1.getUsername() + " not found"));

        contactRepo.delete(contact);
        contactRepo.delete(contact2);

        return contactConv.convertToDto(contact);
    }

}
