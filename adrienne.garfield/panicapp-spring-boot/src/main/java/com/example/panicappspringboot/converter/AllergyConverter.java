package com.example.panicappspringboot.converter;

import com.example.panicappspringboot.dto.AllergyDTO;
import com.example.panicappspringboot.model.Allergy;
import com.example.panicappspringboot.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class AllergyConverter {

    public Allergy convertFromDto(AllergyDTO allergyDTO, User user) {
        Allergy allergy = new Allergy();
        convertFromDto(allergyDTO, allergy, user);
        return allergy;
    }

    public Allergy convertFromDto(AllergyDTO allergyDTO, Allergy allergy, User user) {
        allergy.setIdAllergy(allergyDTO.getAllergyID());
        allergy.setAllergyName(allergyDTO.getAllergyName());
        allergy.setSevereness(allergyDTO.getSevereness());
        allergy.setComments(allergyDTO.getComments());
        allergy.setUser(user);

        return allergy;
    }

    public AllergyDTO convertToDto(Allergy allergy) {
        AllergyDTO allergyDTO = new AllergyDTO();
        convertToDto(allergy, allergyDTO);
        return allergyDTO;
    }

    public AllergyDTO convertToDto(Allergy allergy, AllergyDTO allergyDTO) {
        allergyDTO.setAllergyID(allergy.getIdAllergy());
        allergyDTO.setAllergyName(allergy.getAllergyName());
        allergyDTO.setSevereness(allergy.getSevereness());
        allergyDTO.setComments(allergy.getComments());
        allergyDTO.setUserID(allergy.getUser().getIdUser());

        return allergyDTO;
    }

    public List<AllergyDTO> convertListToDtoList(List<Allergy> allergies) {
        List<AllergyDTO> allergyDTOS = new ArrayList<>();
        allergies.forEach(allergy -> allergyDTOS.add(convertToDto(allergy)));
        return allergyDTOS;
    }

}
