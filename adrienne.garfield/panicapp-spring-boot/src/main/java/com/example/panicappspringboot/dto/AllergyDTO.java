package com.example.panicappspringboot.dto;

public class AllergyDTO {

    private Long allergyID;
    private String allergyName;
    private Integer severeness;
    private String comments;
    private Long userID;

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public Long getAllergyID() {
        return allergyID;
    }

    public void setAllergyID(Long allergyID) {
        this.allergyID = allergyID;
    }

    public String getAllergyName() {
        return allergyName;
    }

    public void setAllergyName(String allergyName) {
        this.allergyName = allergyName;
    }

    public Integer getSevereness() {
        return severeness;
    }

    public void setSevereness(Integer severeness) {
        this.severeness = severeness;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "AllergyDTO{" +
                "allergyID=" + allergyID +
                ", allergyName='" + allergyName + '\'' +
                ", severeness=" + severeness +
                ", comments='" + comments + '\'' +
                ", userID=" + userID +
                '}';
    }
}
