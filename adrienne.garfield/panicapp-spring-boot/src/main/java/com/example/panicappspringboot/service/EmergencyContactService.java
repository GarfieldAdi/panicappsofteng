package com.example.panicappspringboot.service;

import com.example.panicappspringboot.converter.EmContactConverter;
import com.example.panicappspringboot.dto.EmContactDTO;
import com.example.panicappspringboot.model.EmergencyContact;
import com.example.panicappspringboot.model.EmergencyType;
import com.example.panicappspringboot.model.User;
import com.example.panicappspringboot.repository.EmergencyContactRepository;
import com.example.panicappspringboot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class EmergencyContactService {

    @Autowired
    private EmergencyContactRepository emContactRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private EmContactConverter emContactConv;

    public List<EmContactDTO> findEmContactsOfUserByID(Long userID) {
        List<EmergencyContact> emContacts = emContactRepo.findAllByUser_IdUser(userID);
        return emContactConv.convertListToDtoList(emContacts);
    }

    public EmContactDTO findEmContactByID(Long emContactID){
        EmergencyContact emContact = emContactRepo.findById(emContactID).orElseThrow(() ->
                new EntityNotFoundException("Emergency contact with id " + emContactID + " doesn't exist."));
        return emContactConv.convertToDto(emContact);
    }

    @Transactional
    public EmContactDTO insertEmContact(EmContactDTO emContactDTO) {

        Long userID = emContactDTO.getUserID();

        User user = userRepo.findById(userID).orElseThrow(() ->
                new EntityNotFoundException("Can't add emergency contact for user which doesn't exist!"));

        EmergencyContact emContact = emContactConv.convertFromDTO(emContactDTO, user);

        emContactRepo.save(emContact);
        return emContactConv.convertToDto(emContact);
    }

    public EmContactDTO deleteEmContactByID(EmContactDTO emContactDTO) {
        Long emContactID = emContactDTO.getEmContactID();

        EmergencyContact emContact = emContactRepo.findById(emContactID).orElseThrow(() ->
                new EntityNotFoundException("Contact doesn't exist!"));

        emContactRepo.delete(emContact);
        return emContactConv.convertToDto(emContact);
    }

    public EmContactDTO updateEmContact(EmContactDTO emContactDTO) {
        Long emContactID = emContactDTO.getEmContactID();

        EmergencyContact emContact = emContactRepo.findById(emContactID).orElseThrow(() ->
                new EntityNotFoundException("Contact doesn't exist!"));

        emContact.setTelNum(emContactDTO.getTelNum());
        emContact.setNameContact(emContactDTO.getNameContact());
        emContact.setPriority(emContactDTO.getPriority());

        emContactRepo.save(emContact);
        return emContactConv.convertToDto(emContact);
    }

}
