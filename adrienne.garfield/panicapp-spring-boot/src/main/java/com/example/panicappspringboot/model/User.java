package com.example.panicappspringboot.model;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;

@Entity(name = "Users")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "IDUser")
    private Long idUser;

    @Column(name = "Username", length = 45, nullable = false, unique = true)
    private String username;

    @Column(name = "Passw", length = 45, nullable = false)
    private String password;

    @Column(name = "TelNum", length = 45, nullable = false, unique = true)
    private String telNum;

    @Column(name = "BirthDate")
    private Date birthDate;

    @Column(name = "Height")
    private Double height;

    @Column(name = "Weight")
    private Double weight;

    private String image;

    @ManyToOne
    @JoinColumn(name = "UserRoleID", referencedColumnName = "IDRole")
    private Role role;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<EmergencyContact> emContacts;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<Allergy> allergies;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<Sickness> sicknesses;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<EmergencyRequest> emergencyRequests;

    @OneToMany(mappedBy = "user1", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<Contact> contacts1;

    @OneToMany(mappedBy = "user2", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<Contact> contacts2;

    @OneToMany(mappedBy = "senderUser", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<Message> messagesSender;

    @OneToMany(mappedBy = "recipientUser", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<Message> messagesRecipient;

    @OneToMany(mappedBy = "user1", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<Message> messagesUser1;

    @OneToMany(mappedBy = "user2", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<Message> messagesUser2;

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long IDUser) {
        this.idUser = IDUser;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Set<EmergencyContact> getEmContacts() {
        return emContacts;
    }

    public void setEmContacts(Set<EmergencyContact> emContacts) {
        this.emContacts = emContacts;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Set<Allergy> getAllergies() {
        return allergies;
    }

    public void setAllergies(Set<Allergy> allergies) {
        this.allergies = allergies;
    }

    public Set<Sickness> getSicknesses() {
        return sicknesses;
    }

    public void setSicknesses(Set<Sickness> sicknesses) {
        this.sicknesses = sicknesses;
    }

    public Set<EmergencyRequest> getEmRequests() {
        return emergencyRequests;
    }

    public void setEmRequests(Set<EmergencyRequest> emergencyRequests) {
        this.emergencyRequests = emergencyRequests;
    }

    public Set<Contact> getContacts1() {
        return contacts1;
    }

    public void setContacts1(Set<Contact> contacts1) {
        this.contacts1 = contacts1;
    }

    public Set<Contact> getContacts2() {
        return contacts2;
    }

    public void setContacts2(Set<Contact> contacts2) {
        this.contacts2 = contacts2;
    }

    public Set<EmergencyRequest> getEmergencyRequests() {
        return emergencyRequests;
    }

    public void setEmergencyRequests(Set<EmergencyRequest> emergencyRequests) {
        this.emergencyRequests = emergencyRequests;
    }

    public Set<Message> getMessagesSender() {
        return messagesSender;
    }

    public void setMessagesSender(Set<Message> messagesSender) {
        this.messagesSender = messagesSender;
    }

    public Set<Message> getMessagesRecipient() {
        return messagesRecipient;
    }

    public void setMessagesRecipient(Set<Message> messagesRecipient) {
        this.messagesRecipient = messagesRecipient;
    }

    public Set<Message> getMessagesUser1() {
        return messagesUser1;
    }

    public void setMessagesUser1(Set<Message> messagesUser1) {
        this.messagesUser1 = messagesUser1;
    }

    public Set<Message> getMessagesUser2() {
        return messagesUser2;
    }

    public void setMessagesUser2(Set<Message> messagesUser2) {
        this.messagesUser2 = messagesUser2;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
