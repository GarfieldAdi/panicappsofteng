package com.example.panicappspringboot.converter;

import com.example.panicappspringboot.dto.ContactDTO;
import com.example.panicappspringboot.model.Contact;
import org.springframework.stereotype.Component;

@Component
public class ContactConverter {

    public ContactDTO convertToDto(Contact contact) {
        ContactDTO contactDTO = new ContactDTO();
        convertToDto(contact, contactDTO);
        return contactDTO;
    }

    public ContactDTO convertToDto(Contact contact, ContactDTO contactDTO) {
        contactDTO.setContactID(contact.getIdContact());
        contactDTO.setUser1ID(contact.getUser1().getIdUser());
        contactDTO.setUser2ID(contact.getUser2().getIdUser());
        contactDTO.setContactTypeEnum(contact.getContactTypeEnum());

        return contactDTO;
    }
}
