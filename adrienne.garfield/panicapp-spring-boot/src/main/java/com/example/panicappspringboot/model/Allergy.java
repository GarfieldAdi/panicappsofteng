package com.example.panicappspringboot.model;

import javax.persistence.*;

@Entity(name = "Allergies")
public class Allergy {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDAllergy")
    private Long idAllergy;

    @Column(name = "AllergyName", nullable = false)
    private String allergyName;

    @Column(name = "Severeness", nullable = false)
    private Integer severeness;

    @Column(name = "Comments", length = 2000)
    private String comments;

    @ManyToOne
    @JoinColumn(name = "UserID", referencedColumnName = "IDUser")
    private User user;

    public Long getIdAllergy() {
        return idAllergy;
    }

    public void setIdAllergy(Long idAllergy) {
        this.idAllergy = idAllergy;
    }

    public String getAllergyName() {
        return allergyName;
    }

    public void setAllergyName(String allergyName) {
        this.allergyName = allergyName;
    }

    public Integer getSevereness() {
        return severeness;
    }

    public void setSevereness(Integer severeness) {
        this.severeness = severeness;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
