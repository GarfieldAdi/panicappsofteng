package com.example.panicappspringboot.service;

import com.example.panicappspringboot.config.TwilioConfiguration;
import com.example.panicappspringboot.dto.SmsRequest;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.rest.api.v2010.account.MessageCreator;
import com.twilio.type.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TwilioSmsSender{

    private final TwilioConfiguration twilioConfiguration;
    private static final Logger LOGGER = LoggerFactory.getLogger(TwilioSmsSender.class);

    @Autowired
    public TwilioSmsSender(TwilioConfiguration twilioConfiguration) {
        this.twilioConfiguration = twilioConfiguration;
    }

    public void sendSms(SmsRequest smsRequest) {
        PhoneNumber receiver = new PhoneNumber(smsRequest.getPhoneNumber());
        PhoneNumber sender = new PhoneNumber(twilioConfiguration.getTrialNumber());
        String message = smsRequest.getMessage();
        MessageCreator messageCreator = Message.creator(receiver, sender, message);
        messageCreator.create();
        LOGGER.info("Send sms {}", smsRequest);
    }
}
