package com.example.panicappspringboot.repository;

import com.example.panicappspringboot.model.EmergencyContact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmergencyContactRepository extends JpaRepository<EmergencyContact, Long> {

    List<EmergencyContact> findAllByUser_IdUser(Long id);

    List<EmergencyContact> findAllByUser_IdUserAndPriority(Long id, Integer priority);

}
