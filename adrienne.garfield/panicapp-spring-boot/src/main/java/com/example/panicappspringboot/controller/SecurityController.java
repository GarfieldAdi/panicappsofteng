package com.example.panicappspringboot.controller;

import com.example.panicappspringboot.dto.UserDTO;
import com.example.panicappspringboot.service.SecurityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@CrossOrigin("http://localhost:4200")
public class SecurityController {

    @Autowired
    SecurityService securityService;

    @GetMapping(value = "/username", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> currentUserName() {
        UserDTO userDTOResp = securityService.getUsername();
        return new ResponseEntity<>(userDTOResp, HttpStatus.OK);
    }

    @GetMapping(value = "/roles", produces = MediaType.APPLICATION_JSON_VALUE)
    public Set<String> currentRoles() {
        return securityService.getRoles();
    }

}
