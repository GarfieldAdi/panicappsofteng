package com.example.panicappspringboot.model;

import javax.persistence.*;

@Entity(name = "Contacts")
public class Contact {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDContact")
    private Long idContact;

    private ContactType contactType;

    @ManyToOne
    @JoinColumn(name = "User1ID", referencedColumnName = "IDUser")
    private User user1;

    @ManyToOne
    @JoinColumn(name = "User2ID", referencedColumnName = "IDUser")
    private User user2;

    public Long getIdContact() {
        return idContact;
    }

    public void setIdContact(Long idContact) {
        this.idContact = idContact;
    }

    public User getUser1() {
        return user1;
    }

    public void setUser1(User user1) {
        this.user1 = user1;
    }

    public User getUser2() {
        return user2;
    }

    public void setUser2(User user2) {
        this.user2 = user2;
    }

    public ContactType getContactTypeEnum() {
        return contactType;
    }

    public void setContactTypeEnum(ContactType contactType) {
        this.contactType = contactType;
    }
}
