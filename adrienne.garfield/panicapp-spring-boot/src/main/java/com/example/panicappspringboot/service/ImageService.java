package com.example.panicappspringboot.service;

import com.example.panicappspringboot.dto.UserDTO;
import com.example.panicappspringboot.utils.ImageUtils;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
public class ImageService {

    @Autowired
    ImageUtils imageUtils;

    public Map<String, String> getFile(String fileName) {
        try {
            File file = new ClassPathResource("pictures/" + fileName).getFile();
            String encodedImage = Base64.getEncoder().withoutPadding().encodeToString(Files.readAllBytes(file.toPath()));
            Map<String, String> jsonMap = new HashMap<>();
            jsonMap.put("content", encodedImage);
            return jsonMap;
        }
        catch (IOException ioException) {
            System.out.println(ioException);
            throw new RuntimeException("Could not retrieve image!");
        }
    }

    public void saveImageInResources(UserDTO userDTO) {
        try {
            MultipartFile file = userDTO.getFile();
            if (file != null) {
                byte[] bytes = file.getBytes();
                Path path = Paths.get(imageUtils.getImageSource() + userDTO.getFileName());
                Files.write(path, bytes);
            }
        }
        catch (IOException ioException) {
            throw new RuntimeException("Couldn't save image! Try again!");
        }
    }

    public void deleteImageFromResources(UserDTO userDTO) {
        try {
            String fileName = userDTO.getFileName();
            if (!fileName.equals("sample.jpg")) {
                Path path = Paths.get(imageUtils.getImageSource() + fileName);
                Files.delete(path);
            }
        }
        catch (IOException ioException) {
            throw new RuntimeException("Couldn't delete image! Try again!");
        }
    }

    //copied
    String generateUniqueFileName() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) {
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        return salt.toString();
    }

}
