package com.example.panicappspringboot.controller;

import com.example.panicappspringboot.dto.NotificationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("http://localhost:4200")
public class NotificationController {

    @Autowired
    private SimpMessagingTemplate template;

    @GetMapping("/notify")
    public String getNotification() {
        return "Notifications successfully sent to Angular !";
    }
}
