package com.example.panicappspringboot.model;

public enum ContactType {

    PENDING,
    REQUESTED,
    FRIENDS,
    REFUSED

}
