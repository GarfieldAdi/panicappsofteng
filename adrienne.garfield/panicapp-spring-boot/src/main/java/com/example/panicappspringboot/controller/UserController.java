package com.example.panicappspringboot.controller;

import com.example.panicappspringboot.dto.EmRequestDTO;
import com.example.panicappspringboot.dto.GeoLocationDTO;
import com.example.panicappspringboot.dto.PasswordChangeDTO;
import com.example.panicappspringboot.dto.UserDTO;
import com.example.panicappspringboot.model.User;
import com.example.panicappspringboot.service.GeoLocationService;
import com.example.panicappspringboot.service.SecurityService;
import com.example.panicappspringboot.service.UserService;
import com.example.panicappspringboot.utils.AlertStorage;
import com.google.gson.Gson;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/users")
public class UserController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    UserService userService;

    @Autowired
    SecurityService securityService;

    @Autowired
    GeoLocationService geoLocationService;

    @Autowired
    AlertStorage alertStorage;

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDTO>> getUsers(HttpServletRequest request) {
        LOGGER.info("Find all users");
        List<UserDTO> userDTOS = userService.findAllUsers();
        LOGGER.info("Found successfully all users: {}", userDTOS);
        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @GetMapping(path = "/username/{username}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> getUserByUsername(@PathVariable String username) {
        LOGGER.info("Find user with username: " + username);
        UserDTO userDTOResp = userService.findUserByUsername(username);
        LOGGER.info("Found successfully user with username: " + username);
        return new ResponseEntity<>(userDTOResp, HttpStatus.OK);
    }

    @GetMapping(path = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> getUser(@PathVariable Long id) {
        LOGGER.info("Find user with id: " + id);
        /*if (!securityService.authorized(id)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }*/
        UserDTO userDTOResp = userService.findUserByID(id);
        LOGGER.info("Found successfully user with id: " + id);
        return new ResponseEntity<>(userDTOResp, HttpStatus.OK);
    }

    @GetMapping(path = "/admins", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDTO>> adminList() {
        LOGGER.info("Find all admin users");
        List<UserDTO> userDTOS = userService.findAllAdminUsers();
        LOGGER.info("Found successfully all admin users: {}", userDTOS);
        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @GetMapping(path = "/geoLocation", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<GeoLocationDTO> userLocation(HttpServletRequest request) {
        LOGGER.info("Find the location of the user");
        GeoLocationDTO geoLocationDTO = geoLocationService.getLocation(request);
        LOGGER.info("Found successfully the location of the user {}", geoLocationDTO);
        return new ResponseEntity<>(geoLocationDTO, HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> addUser(@RequestPart("user") UserDTO userDTO, @RequestPart(value = "file", required = false) MultipartFile file) {
        LOGGER.info("Insert new user with username: {}", userDTO.getUsername());
        userDTO.setFile(file);
        UserDTO userDTOResp = userService.insertUser(userDTO);
        LOGGER.info("Inserted successfully new user: {}", userDTOResp);
        return new ResponseEntity<>(userDTOResp, HttpStatus.CREATED);
    }

    @PutMapping(consumes = MediaType.MULTIPART_FORM_DATA_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> updateUser(@RequestPart("user") UserDTO userDTO, @RequestPart(value = "file", required = false) MultipartFile file) {
        LOGGER.info("Update user with id: {}", userDTO.getUserID());
        if (!securityService.authorized(userDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        UserDTO userDTOResp = userService.updateUser(userDTO, file);
        LOGGER.info("Updated successfully new user: {}", userDTOResp);
        return new ResponseEntity<>(userDTOResp, HttpStatus.CREATED);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<UserDTO> deleteUser(@RequestBody UserDTO userDTO) {
        LOGGER.info("Delete user with username: {}", userDTO.getUsername());
        if (!securityService.authorized(userDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        UserDTO userDTOResp = userService.deleteUser(userDTO);
        LOGGER.info("Deleted successfully user: {}", userDTOResp);
        return new ResponseEntity<>(userDTOResp, HttpStatus.OK);
    }

    @PutMapping(path = "/resetPassword")
    public ResponseEntity<Gson> resetPassword(@RequestBody PasswordChangeDTO passwDTO) {
        LOGGER.info("Changing password with token");
        userService.resetPassword(passwDTO);
        LOGGER.info("Successfully changed password");
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(path = "/generateToken")
    public ResponseEntity<Gson> generateToken(@RequestBody UserDTO userDTO) {
        LOGGER.info("Generating token for tel nr: {}", userDTO.getTelNum());
        userService.generateToken(userDTO);
        LOGGER.info("Successfully generated token for tel nr: {}", userDTO.getTelNum());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping(path = "/acceptNotification")
    public ResponseEntity<Gson> acceptNotification(@RequestBody EmRequestDTO emRequestDTO) {
        LOGGER.info("Accepting notification with id {}", emRequestDTO.getEmRequestID());
        alertStorage.acceptNotification(emRequestDTO);
        LOGGER.info("Successfully accepted notification with id {}", emRequestDTO.getEmRequestID());
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
