package com.example.panicappspringboot.dto;

import com.example.panicappspringboot.model.ContactType;

public class ContactDTO {

    private Long contactID;
    private Long user1ID;
    private Long user2ID;
    private ContactType contactType;

    public Long getContactID() {
        return contactID;
    }

    public void setContactID(Long contactID) {
        this.contactID = contactID;
    }

    public Long getUser1ID() {
        return user1ID;
    }

    public void setUser1ID(Long user1ID) {
        this.user1ID = user1ID;
    }

    public Long getUser2ID() {
        return user2ID;
    }

    public void setUser2ID(Long user2ID) {
        this.user2ID = user2ID;
    }

    public ContactType getContactTypeEnum() {
        return contactType;
    }

    public void setContactTypeEnum(ContactType contactType) {
        this.contactType = contactType;
    }

    @Override
    public String toString() {
        return "ContactDTO{" +
                "contactID=" + contactID +
                ", user1ID=" + user1ID +
                ", user2ID=" + user2ID +
                ", contactType=" + contactType +
                '}';
    }
}
