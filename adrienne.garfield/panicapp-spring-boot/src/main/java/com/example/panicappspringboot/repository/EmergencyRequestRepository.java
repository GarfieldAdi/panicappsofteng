package com.example.panicappspringboot.repository;

import com.example.panicappspringboot.model.EmergencyRequest;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmergencyRequestRepository extends JpaRepository<EmergencyRequest, Long> {

    List<EmergencyRequest> findAllByUser_IdUser(Long id);

}
