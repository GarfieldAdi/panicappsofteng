package com.example.panicappspringboot.model;

import javax.persistence.*;
import java.util.Date;

@Entity(name = "Requests")
public class EmergencyRequest {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDRequest")
    private Long idRequest;

    @Column(name = "StartDate")
    private Date startDate;

    @Column(name = "EndDate")
    private Date endDate;

    @Column(name = "TimeInt")
    private Double timeInt;

    @ManyToOne
    @JoinColumn(name = "UserID", referencedColumnName = "IDUser")
    private User user;

    private EmergencyType emType;

    public Long getIdRequest() {
        return idRequest;
    }

    public void setIdRequest(Long idRequest) {
        this.idRequest = idRequest;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Double getTimeInt() {
        return timeInt;
    }

    public void setTimeInt(Double timeInt) {
        this.timeInt = timeInt;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public EmergencyType getEmType() {
        return emType;
    }

    public void setEmType(EmergencyType emType) {
        this.emType = emType;
    }
}
