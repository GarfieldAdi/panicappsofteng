package com.example.panicappspringboot.controller;

import com.example.panicappspringboot.dto.ContactDTO;
import com.example.panicappspringboot.dto.MessageDTO;
import com.example.panicappspringboot.service.MessageService;
import com.example.panicappspringboot.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/messages")
public class MessageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactController.class);

    @Autowired
    MessageService messageService;

    @Autowired
    SecurityService securityService;

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageDTO> sendMessage(@RequestBody MessageDTO messageDTO) {
        LOGGER.info("User with id {} sent message to user with id {}", messageDTO.getSenderID(), messageDTO.getRecipientID());
        MessageDTO messageDTOResp = messageService.sendMessage(messageDTO);
        LOGGER.info("User with id {} sent message to user with id {}", messageDTO.getSenderID(), messageDTO.getRecipientID());
        return new ResponseEntity<>(messageDTOResp, HttpStatus.OK);
    }

    @PostMapping(path = "/users", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MessageDTO>> getMessagesOfUsers(@RequestBody List<MessageDTO> messageDTOS) {
        LOGGER.info("Filters message list");
        List<MessageDTO> messageDTOResps = new ArrayList<>();
        messageDTOS.forEach(messageDTO -> {
            Optional<MessageDTO> messageDTOResp = messageService.getOneMessageBetweenUsers(messageDTO);
            if (messageDTOResp.isPresent()) {
                messageDTOResps.add(messageDTOResp.orElseThrow());
            }
        });
        LOGGER.info("Successfully filtered messages");
        return new ResponseEntity<>(messageDTOResps, HttpStatus.OK);
    }

    @PostMapping(path = "/conversation", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<MessageDTO>> getConversation(@RequestBody MessageDTO messageDTO) {
        LOGGER.info("Filters message list");
        List<MessageDTO> messageDTOS = messageService.getMessagesBetweenUsers(messageDTO);
        LOGGER.info("Successfully got list of messages");
        return new ResponseEntity<>(messageDTOS, HttpStatus.OK);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MessageDTO> deleteMessage(@RequestBody MessageDTO messageDTO) {
        LOGGER.info("Deletes messagse between users with ids: {}, {}", messageDTO.getUser1ID(), messageDTO.getUser2ID());
        MessageDTO messageDTOResp = messageService.deleteMessage(messageDTO);
        LOGGER.info("Successfully deleted messages between users with ids: {}, {}", messageDTO.getUser1ID(), messageDTO.getUser2ID());
        return new ResponseEntity<>(messageDTOResp, HttpStatus.OK);
    }

}
