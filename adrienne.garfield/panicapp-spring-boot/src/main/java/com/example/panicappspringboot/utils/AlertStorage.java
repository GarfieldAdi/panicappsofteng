package com.example.panicappspringboot.utils;

import com.example.panicappspringboot.converter.EmRequestConverter;
import com.example.panicappspringboot.dto.EmRequestDTO;
import com.example.panicappspringboot.dto.NotificationDTO;
import com.example.panicappspringboot.model.EmergencyRequest;
import com.example.panicappspringboot.repository.EmergencyRequestRepository;
import com.example.panicappspringboot.service.SecurityService;
import com.example.panicappspringboot.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@Component
@EnableAsync
@EnableScheduling
public class AlertStorage {

    private static final Logger LOGGER = LoggerFactory.getLogger(AlertStorage.class);
    private ConcurrentHashMap<Long, NotificationDTO> alertsWithID = new ConcurrentHashMap<>();

    @Autowired
    private SimpMessagingTemplate template;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private EmRequestConverter emRequestConverter;

    @Autowired
    private EmergencyRequestRepository emRequestRepo;

    @Autowired
    private UserService userService;

    private boolean loggedIn;
    private NotificationDTO notificationDTO;

    public AlertStorage() {
        loggedIn = false;
    }

    public void setOnLogin() {
        loggedIn = true;
        notificationDTO = new NotificationDTO();
    }

    public void setOnLogout() {
        loggedIn = false;
    }

    public void acceptNotification(EmRequestDTO emRequestDTO) {
        LOGGER.info("Accepted alert for emergency");
        NotificationDTO notificationDTO = alertsWithID.get(emRequestDTO.getEmRequestID());
        if (notificationDTO == null) {
            notificationDTO = new NotificationDTO();
            notificationDTO.setEmRequestID(emRequestDTO.getEmRequestID());
            notificationDTO.setEmRequestType(emRequestDTO.getEmType().toString());
        }
        notificationDTO.setUnAnswered(0);
        alertsWithID.put(emRequestDTO.getEmRequestID(), notificationDTO);
    }

    public boolean pastRequest(EmRequestDTO emRequestDTO) {
        Date now = new Date();
        long timeDiff = now.getTime() - emRequestDTO.getEndDate().getTime();
        return timeDiff >= 0;
    }

    public boolean timeIntervalPassed(EmRequestDTO emRequestDTO) {
        Date now = new Date();
        long timeDiff = now.getTime() - emRequestDTO.getStartDate().getTime();
        long minuteDiff = TimeUnit.MILLISECONDS.toMinutes(timeDiff);
        return minuteDiff % emRequestDTO.getTimeInt() == 0;
    }

    public void handlePastRequest(EmRequestDTO emRequestDTO) {
        NotificationDTO notificationDTO = alertsWithID.get(emRequestDTO.getEmRequestID());
        if (notificationDTO != null) {
            Integer unAnswered = notificationDTO.getUnAnswered();
            if (unAnswered == 0) {
                alertsWithID.remove(emRequestDTO.getEmRequestID());
            }
            else {
                if (unAnswered <= 10) {
                    notificationDTO.setUnAnswered(unAnswered + 1);
                }
                if (unAnswered >= 5 && unAnswered < 10) {
                    userService.sendEmergencySms(10 - unAnswered, notificationDTO, securityService.getUserDTO());
                }
                alertsWithID.put(emRequestDTO.getEmRequestID(), notificationDTO);
                template.convertAndSend("/topic/notification", notificationDTO);
                LOGGER.info("Alert for {} sent to front-end", emRequestDTO.getEmType());
            }
        }
    }

    public void handleRequest(EmRequestDTO emRequestDTO) {
        NotificationDTO notificationDTO = alertsWithID.get(emRequestDTO.getEmRequestID());

        if (notificationDTO == null) {
            notificationDTO = new NotificationDTO();
            notificationDTO.setEmRequestID(emRequestDTO.getEmRequestID());
            notificationDTO.setEmRequestType(emRequestDTO.getEmType().toString());
            notificationDTO.setUnAnswered(1);
        }
        else {
            Integer unAnswered = notificationDTO.getUnAnswered();
            if (unAnswered <= 10) {
                notificationDTO.setUnAnswered(unAnswered + 1);
            }
            if (unAnswered >= 5 && unAnswered < 10) {
                userService.sendEmergencySms(10 - unAnswered, notificationDTO, securityService.getUserDTO());
            }
        }

        alertsWithID.put(emRequestDTO.getEmRequestID(), notificationDTO);
        template.convertAndSend("/topic/notification", notificationDTO);
        LOGGER.info("Alert for {} sent to front-end", emRequestDTO.getEmType());
    }

    @Async
    @Scheduled(cron = "*/30 * * * * *")
    public void verifyAlerts() {
        if (loggedIn) {
            List<EmergencyRequest> emRequests = emRequestRepo.findAllByUser_IdUser(securityService.getUserDTO().getUserID());
            List<EmRequestDTO> emRequestDTOS = emRequestConverter.convertListToDtoList(emRequests);

            emRequestDTOS.forEach(emRequestDTO -> {
                if (!pastRequest(emRequestDTO) && timeIntervalPassed(emRequestDTO)) {
                    handleRequest(emRequestDTO);
                }
                else {
                    handlePastRequest(emRequestDTO);
                }
            });
        }
    }
}
