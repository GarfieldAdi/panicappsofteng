package com.example.panicappspringboot.utils;

import java.util.Date;

public class Token {

    private Long userID;
    private String token;
    private Date creationalDate;

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getCreationalDate() {
        return creationalDate;
    }

    public void setCreationalDate(Date creationalDate) {
        this.creationalDate = creationalDate;
    }
}
