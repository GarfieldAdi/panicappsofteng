package com.example.panicappspringboot.converter;

import com.example.panicappspringboot.dto.UserDTO;
import com.example.panicappspringboot.model.Role;
import com.example.panicappspringboot.model.User;
import com.example.panicappspringboot.utils.ImageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserConverter {

    public User convertFromDto(UserDTO userDTO, Role role) {
        User user = new User();
        convertFromDto(userDTO, user, role);
        return user;
    }

    public User convertFromDto(UserDTO userDTO, User user, Role role) {
        user.setIdUser(userDTO.getUserID());
        user.setUsername(userDTO.getUsername());
        user.setPassword(userDTO.getPassw());
        user.setBirthDate(userDTO.getBirthDate());
        user.setHeight(userDTO.getHeight());
        user.setWeight(userDTO.getWeight());
        user.setTelNum(userDTO.getTelNum());
        user.setRole(role);
        if (userDTO.getFile() == null) {
            user.setImage("sample.jpg");
        }
        else {
            user.setImage(userDTO.getFileName());
        }
        return user;
    }

    public UserDTO convertToDto(User user) {
        UserDTO userDTO = new UserDTO();
        convertToDto(user, userDTO);
        return userDTO;
    }

    public UserDTO convertToDto(User user, UserDTO userDTO) {
        userDTO.setUserID(user.getIdUser());
        userDTO.setUsername(user.getUsername());
        userDTO.setPassw(user.getPassword());
        userDTO.setBirthDate(user.getBirthDate());
        userDTO.setHeight(user.getHeight());
        userDTO.setWeight(user.getWeight());
        userDTO.setRoleID(user.getRole().getIdRole());
        userDTO.setTelNum(user.getTelNum());
        userDTO.setFileName(user.getImage());
        return userDTO;
    }

    public List<UserDTO> convertListToDtoList(List<User> users) {
        List<UserDTO> userDTOS = new ArrayList<>();
        users.forEach(user -> userDTOS.add(convertToDto(user)));
        return userDTOS;
    }

}
