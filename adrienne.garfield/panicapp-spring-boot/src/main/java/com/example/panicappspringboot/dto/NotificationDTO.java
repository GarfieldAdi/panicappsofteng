package com.example.panicappspringboot.dto;

public class NotificationDTO {

    private Long emRequestID;
    private String emRequestType;
    private Integer unAnswered;

    public Long getEmRequestID() {
        return emRequestID;
    }

    public void setEmRequestID(Long emRequestID) {
        this.emRequestID = emRequestID;
    }

    public String getEmRequestType() {
        return emRequestType;
    }

    public void setEmRequestType(String emRequestType) {
        this.emRequestType = emRequestType;
    }

    public Integer getUnAnswered() {
        return unAnswered;
    }

    public void setUnAnswered(Integer unAnswered) {
        this.unAnswered = unAnswered;
    }
}
