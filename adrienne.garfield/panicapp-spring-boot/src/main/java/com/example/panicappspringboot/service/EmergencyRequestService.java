package com.example.panicappspringboot.service;

import com.example.panicappspringboot.converter.EmRequestConverter;
import com.example.panicappspringboot.dto.EmRequestDTO;
import com.example.panicappspringboot.model.EmergencyRequest;
import com.example.panicappspringboot.model.User;
import com.example.panicappspringboot.repository.EmergencyRequestRepository;
import com.example.panicappspringboot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class EmergencyRequestService {

    @Autowired
    private EmergencyRequestRepository emRequestRepo;

    @Autowired
    private EmRequestConverter emRequestConverter;

    @Autowired
    private UserRepository userRepo;

    public List<EmRequestDTO> findEmRequestsOfUserByID(Long userID) {
        List<EmergencyRequest> emRequests = emRequestRepo.findAllByUser_IdUser(userID);
        return emRequestConverter.convertListToDtoList(emRequests);
    }

    @Transactional
    public EmRequestDTO insertEmRequest(EmRequestDTO emRequestDTO) {

        Long userID = emRequestDTO.getUserID();

        System.out.println(emRequestDTO.getUserID());
        User user = userRepo.findById(userID).orElseThrow(() ->
                new EntityNotFoundException("User with id " + userID + " doesn't exist!"));

        EmergencyRequest emRequest = emRequestConverter.convertFromDto(emRequestDTO, user);
        emRequestRepo.save(emRequest);

        return emRequestConverter.convertToDto(emRequest);
    }

    public EmRequestDTO updateEmRequest(EmRequestDTO emRequestDTO) {

        Long emRequestID = emRequestDTO.getEmRequestID();

        EmergencyRequest emRequest = emRequestRepo.findById(emRequestID).orElseThrow(() ->
                new EntityNotFoundException("Request with id " + emRequestID + " doesn't exist!"));

        emRequest.setEndDate(emRequestDTO.getEndDate());
        emRequest.setTimeInt(emRequestDTO.getTimeInt());
        emRequest.setEmType(emRequestDTO.getEmType());

        return emRequestConverter.convertToDto(emRequest);
    }

    public EmRequestDTO deleteEmRequest(EmRequestDTO emRequestDTO) {

        Long emRequestID = emRequestDTO.getEmRequestID();
        System.out.println(emRequestID);

        EmergencyRequest emRequest = emRequestRepo.findById(emRequestID).orElseThrow(() ->
                new EntityNotFoundException("Request with id " + emRequestID + " doesn't exist!"));
        emRequestRepo.delete(emRequest);

        return emRequestConverter.convertToDto(emRequest);
    }

}
