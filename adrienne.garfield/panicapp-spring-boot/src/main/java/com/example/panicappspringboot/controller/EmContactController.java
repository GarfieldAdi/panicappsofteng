package com.example.panicappspringboot.controller;

import com.example.panicappspringboot.dto.EmContactDTO;
import com.example.panicappspringboot.service.EmergencyContactService;
import com.example.panicappspringboot.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/emergencyContacts")
public class EmContactController {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmContactController.class);

    @Autowired
    EmergencyContactService emContactService;

    @Autowired
    SecurityService securityService;

    @GetMapping(path = "/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EmContactDTO>> getEmContactsOfUser(@PathVariable Long userID) {
        LOGGER.info("Find emergency contacts of user with id: " + userID);
        if (!securityService.authorized(userID)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        List<EmContactDTO> emContactDTOs = emContactService.findEmContactsOfUserByID(userID);
        LOGGER.info("Found successfully emergency contacts of user with id: " + userID);
        return new ResponseEntity<>(emContactDTOs, HttpStatus.OK);
    }

    @GetMapping(path = "/byID/{emContactID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmContactDTO> getEmContact(@PathVariable Long emContactID) {
        LOGGER.info("Find emergency contact with id: " + emContactID);
        EmContactDTO emContactDTOResp = emContactService.findEmContactByID(emContactID);
        if (!securityService.authorized(emContactDTOResp.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        LOGGER.info("Found successfully emergency contact with id: " + emContactID);
        return new ResponseEntity<>(emContactDTOResp, HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmContactDTO> addEmContact(@RequestBody EmContactDTO emContactDTO) {
        LOGGER.info("Inserts new emergency contact for user with id = {}", emContactDTO.getUserID());
        if (!securityService.authorized(emContactDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        EmContactDTO emContactDTOResp = emContactService.insertEmContact(emContactDTO);
        LOGGER.info("Inserted succesfully entity: {}", emContactDTOResp);
        return new ResponseEntity<>(emContactDTOResp, HttpStatus.CREATED);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmContactDTO> deleteEmContact(@RequestBody EmContactDTO emContactDTO) {
        LOGGER.info("Deletes emergency contact with id = {}", emContactDTO.getEmContactID());
        if (!securityService.authorized(emContactDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        EmContactDTO emContactDTOResp = emContactService.deleteEmContactByID(emContactDTO);
        LOGGER.info("Deleted succesfully entity: {}", emContactDTOResp);
        return new ResponseEntity<>(emContactDTOResp, HttpStatus.OK);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmContactDTO> updateEmContactTelNum(@RequestBody EmContactDTO emContactDTO) {
        LOGGER.info("Updates emergency contact with id = {}", emContactDTO.getEmContactID());
        if (!securityService.authorized(emContactDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        EmContactDTO emContactDTOResp = emContactService.updateEmContact(emContactDTO);
        LOGGER.info("Updated succesfully entity: {}", emContactDTOResp);
        return new ResponseEntity<>(emContactDTOResp, HttpStatus.CREATED);
    }

}
