package com.example.panicappspringboot.model;

public enum EmergencyType {

    HIKING,
    WALKING,
    FIGHT,
    ELDERLY

}
