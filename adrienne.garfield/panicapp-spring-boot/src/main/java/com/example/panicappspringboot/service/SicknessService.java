package com.example.panicappspringboot.service;

import com.example.panicappspringboot.converter.SicknessConverter;
import com.example.panicappspringboot.dto.SicknessDTO;
import com.example.panicappspringboot.dto.SicknessDTO;
import com.example.panicappspringboot.model.Sickness;
import com.example.panicappspringboot.model.Sickness;
import com.example.panicappspringboot.model.User;
import com.example.panicappspringboot.repository.SicknessRepository;
import com.example.panicappspringboot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class SicknessService {

    @Autowired
    private SicknessRepository sicknessRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private SicknessConverter sicknessConv;

    public List<SicknessDTO> findSicknessesOfUserByID(Long userID) {
        List<Sickness> allergies = sicknessRepo.findAllByUser_IdUser(userID);
        return sicknessConv.convertListToDtoList(allergies);
    }

    @Transactional
    public SicknessDTO insertSickness(SicknessDTO sicknessDTO) {
        Long userID = sicknessDTO.getUserID();

        User user = userRepo.findById(userID).orElseThrow(() ->
                new EntityNotFoundException("User doesn't exist!"));

        Sickness sickness = new Sickness();
        sickness.setSicknessName(sicknessDTO.getSicknessName());
        sickness.setComments(sicknessDTO.getComments());
        sickness.setSevereness(sicknessDTO.getSevereness());
        sickness.setUser(user);

        sicknessRepo.save(sickness);
        return sicknessConv.convertToDto(sickness);

    }

    public SicknessDTO deleteSicknessByID(SicknessDTO sicknessDTO) {
        Long sicknessID = sicknessDTO.getSicknessID();

        Sickness sickness = sicknessRepo.findById(sicknessID).orElseThrow(() ->
                new EntityNotFoundException("Cannot delete sickness if it doesn't exist!"));

        sicknessRepo.delete(sickness);
        return sicknessConv.convertToDto(sickness);
    }

    public SicknessDTO updateSickness(SicknessDTO sicknessDTO) {

        Long sicknessID = sicknessDTO.getSicknessID();

        Sickness sickness = sicknessRepo.findById(sicknessID).orElseThrow(() ->
                new EntityNotFoundException("Cannot update sickness if it doesn't exist!"));

        sickness.setSicknessName(sicknessDTO.getSicknessName());
        sickness.setComments(sicknessDTO.getComments());
        sickness.setSevereness(sicknessDTO.getSevereness());
        sicknessRepo.save(sickness);

        return sicknessConv.convertToDto(sickness);
    }


}
