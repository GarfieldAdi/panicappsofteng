package com.example.panicappspringboot.model;

import javax.persistence.*;

@Entity(name = "EmergencyContacts")
public class EmergencyContact {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDContact")
    private Long idContact;

    @Column(name = "NameContact")
    private String nameContact;

    @Column(name = "TelNum")
    private String telNum;

    @Column(name = "Priority")
    private Integer priority;

    @ManyToOne
    @JoinColumn(name = "UserID", referencedColumnName = "IDUser")
    private User user;

    public Long getIdContact() {
        return idContact;
    }

    public void setIdContact(Long idContact) {
        this.idContact = idContact;
    }

    public String getNameContact() {
        return nameContact;
    }

    public void setNameContact(String nameContact) {
        this.nameContact = nameContact;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
