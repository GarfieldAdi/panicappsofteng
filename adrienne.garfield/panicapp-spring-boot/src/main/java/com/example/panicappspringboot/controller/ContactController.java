package com.example.panicappspringboot.controller;

import com.example.panicappspringboot.dto.ContactDTO;
import com.example.panicappspringboot.dto.EmRequestDTO;
import com.example.panicappspringboot.dto.UserDTO;
import com.example.panicappspringboot.service.ContactService;
import com.example.panicappspringboot.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/contacts")
public class ContactController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactController.class);

    @Autowired
    ContactService contactService;

    @Autowired
    SecurityService securityService;

    @GetMapping(path = "/friends/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDTO>> getFriendsOfUser(@PathVariable Long userID) {
        LOGGER.info("Find friends of user with id: " + userID);
        if (!securityService.authorized(userID)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        List<UserDTO> userDTOS = contactService.findFriendsOfUser(userID);
        LOGGER.info("Found successfully friends of user with id: " + userID);
        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @GetMapping(path = "/requests/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<UserDTO>> getRequestsOfUser(@PathVariable Long userID) {
        LOGGER.info("Find requests of user with id: " + userID);
        if (!securityService.authorized(userID)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        List<UserDTO> userDTOS = contactService.findRequestsOfUser(userID);
        LOGGER.info("Found successfully requests of user with id: " + userID);
        return new ResponseEntity<>(userDTOS, HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactDTO> addContact(@RequestBody ContactDTO contactDTO) {
        LOGGER.info("Adds request between users with ids: {}, {}", contactDTO.getUser1ID(), contactDTO.getUser2ID());
        ContactDTO contactDTOResp = contactService.addContact(contactDTO);
        LOGGER.info("Successfully added request between users with ids: {}, {}", contactDTO.getUser1ID(), contactDTO.getUser2ID());
        return new ResponseEntity<>(contactDTOResp, HttpStatus.CREATED);
    }

    @PutMapping(path = "/refused", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactDTO> refuseContact(@RequestBody ContactDTO contactDTO) {
        LOGGER.info("Refuses request between users with ids: {}, {}", contactDTO.getUser1ID(), contactDTO.getUser2ID());
        ContactDTO contactDTOResp = contactService.refuseContact(contactDTO);
        LOGGER.info("Successfully refused request between users with ids: {}, {}", contactDTO.getUser1ID(), contactDTO.getUser2ID());
        return new ResponseEntity<>(contactDTOResp, HttpStatus.OK);
    }

    @PutMapping(path = "/accepted", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactDTO> acceptContact(@RequestBody ContactDTO contactDTO) {
        LOGGER.info("Accepts request between users with ids: {}, {}", contactDTO.getUser1ID(), contactDTO.getUser2ID());
        ContactDTO contactDTOResp = contactService.acceptContact(contactDTO);
        LOGGER.info("Successfully accepted request between users with ids: {}, {}", contactDTO.getUser1ID(), contactDTO.getUser2ID());
        return new ResponseEntity<>(contactDTOResp, HttpStatus.OK);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ContactDTO> deleteContact(@RequestBody ContactDTO contactDTO) {
        LOGGER.info("Deletes request between users with ids: {}, {}", contactDTO.getUser1ID(), contactDTO.getUser2ID());
        ContactDTO contactDTOResp = contactService.deleteContact(contactDTO);
        LOGGER.info("Successfully deleted relationship between users with ids: {}, {}", contactDTO.getUser1ID(), contactDTO.getUser2ID());
        return new ResponseEntity<>(contactDTOResp, HttpStatus.OK);
    }

}
