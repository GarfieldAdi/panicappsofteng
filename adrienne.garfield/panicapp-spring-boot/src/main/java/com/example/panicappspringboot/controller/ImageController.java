package com.example.panicappspringboot.controller;

import com.example.panicappspringboot.service.ImageService;
import com.example.panicappspringboot.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("/images")
public class ImageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ImageController.class);

    @Autowired
    ImageService imageService;

    @GetMapping(path = "/{fileName}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Map<String, String>> getUser(@PathVariable String fileName) {
        LOGGER.info("Find file with name: " + fileName);
        Map<String, String> resp = imageService.getFile(fileName);
        LOGGER.info("Found successfully file with name: " + fileName);
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

}
