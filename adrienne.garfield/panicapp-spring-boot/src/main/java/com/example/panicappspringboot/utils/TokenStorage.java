package com.example.panicappspringboot.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

@Component
@EnableAsync
@EnableScheduling
public class TokenStorage {

    private ConcurrentHashMap<String, Token> tokensWithID = new ConcurrentHashMap<>();
    private static final Logger LOGGER = LoggerFactory.getLogger(TokenStorage.class);

    public void addTokenInStorage(Token token) {
        tokensWithID.put(token.getToken(), token);
    }

    public Optional<Token> getTokenWithKey(String tokenKey) {
        return Optional.of(tokensWithID.get(tokenKey));
    }

    @Async
    @Scheduled(cron = "0 * * * * *")
    public void clearExpiredTokens() {
        tokensWithID.forEach((tokenString, token) -> {
            Date now = new Date();
            long timeDiff = now.getTime() - token.getCreationalDate().getTime();
            long minuteDiff = TimeUnit.MILLISECONDS.toMinutes(timeDiff);
            if (minuteDiff > 5) {
                LOGGER.info("A token expired");
                tokensWithID.remove(tokenString);
            }
        });
    }

}
