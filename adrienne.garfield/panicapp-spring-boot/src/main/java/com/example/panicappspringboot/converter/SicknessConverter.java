package com.example.panicappspringboot.converter;

import com.example.panicappspringboot.dto.SicknessDTO;
import com.example.panicappspringboot.model.Sickness;
import com.example.panicappspringboot.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SicknessConverter {

    public Sickness convertFromDto(SicknessDTO sicknessDTO, User user) {
        Sickness sickness = new Sickness();
        convertFromDto(sicknessDTO, sickness, user);
        return sickness;
    }

    public Sickness convertFromDto(SicknessDTO sicknessDTO, Sickness sickness, User user) {
        sickness.setIdSickness(sicknessDTO.getSicknessID());
        sickness.setSicknessName(sicknessDTO.getSicknessName());
        sickness.setSevereness(sicknessDTO.getSevereness());
        sickness.setComments(sicknessDTO.getComments());
        sickness.setUser(user);

        return sickness;
    }

    public SicknessDTO convertToDto(Sickness sickness) {
        SicknessDTO sicknessDTO = new SicknessDTO();
        convertToDto(sickness, sicknessDTO);
        return sicknessDTO;
    }

    public SicknessDTO convertToDto(Sickness sickness, SicknessDTO sicknessDTO) {
        sicknessDTO.setSicknessID(sickness.getIdSickness());
        sicknessDTO.setSicknessName(sickness.getSicknessName());
        sicknessDTO.setSevereness(sickness.getSevereness());
        sicknessDTO.setComments(sickness.getComments());
        sicknessDTO.setUserID(sickness.getUser().getIdUser());

        return sicknessDTO;
    }

    public List<SicknessDTO> convertListToDtoList(List<Sickness> sicknesses) {
        List<SicknessDTO> sicknessDTOS = new ArrayList<>();
        sicknesses.forEach(sickness -> sicknessDTOS.add(convertToDto(sickness)));
        return sicknessDTOS;
    }
    
}
