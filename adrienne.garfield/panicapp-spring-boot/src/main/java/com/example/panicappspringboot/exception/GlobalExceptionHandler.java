package com.example.panicappspringboot.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.util.Arrays;

@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<ExceptionDetails> handleEntityNotFoundExceptions(EntityNotFoundException e, WebRequest webRequest) {
        ExceptionDetails exceptionDet = new ExceptionDetails(
                HttpStatus.NOT_FOUND.value(),
                e.getMessage(),
                HttpStatus.NOT_FOUND.getReasonPhrase(),
                HttpStatus.NOT_FOUND.series().name(),
                webRequest.getDescription(false));

        LOGGER.error(exceptionDet.getPath() + " responded with stack strace " + Arrays.toString(e.getStackTrace()));
        return new ResponseEntity<>(exceptionDet, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(EntityExistsException.class)
    public ResponseEntity<ExceptionDetails> handleEntityExists(EntityExistsException e, WebRequest webRequest) {
        ExceptionDetails exceptionDet = new ExceptionDetails(
                HttpStatus.CONFLICT.value(),
                e.getMessage(),
                HttpStatus.CONFLICT.getReasonPhrase(),
                HttpStatus.CONFLICT.series().name(),
                webRequest.getDescription(false));

        LOGGER.error(exceptionDet.getPath() + " responded with stack strace " + Arrays.toString(e.getStackTrace()));
        return new ResponseEntity<>(exceptionDet, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ExceptionDetails> handleRunTimeExceptions(RuntimeException e, WebRequest webRequest) {
        ExceptionDetails exceptionDet = new ExceptionDetails(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                e.getMessage(),
                HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                HttpStatus.INTERNAL_SERVER_ERROR.series().name(),
                webRequest.getDescription(false));

        LOGGER.error(exceptionDet.getPath() + " responded with stack strace " + Arrays.toString(e.getStackTrace()));
        return new ResponseEntity<>(exceptionDet, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ExceptionDetails> handleAllExceptions(Exception e, WebRequest webRequest) {
        ExceptionDetails exceptionDet = new ExceptionDetails(
                HttpStatus.INTERNAL_SERVER_ERROR.value(),
                e.getMessage(),
                HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),
                HttpStatus.INTERNAL_SERVER_ERROR.series().name(),
                webRequest.getDescription(false));

        LOGGER.error(exceptionDet.getPath() + " responded with stack strace " + Arrays.toString(e.getStackTrace()));
        return new ResponseEntity<>(exceptionDet, HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
