package com.example.panicappspringboot.converter;

import com.example.panicappspringboot.dto.MessageDTO;
import com.example.panicappspringboot.dto.UserDTO;
import com.example.panicappspringboot.model.Message;
import com.example.panicappspringboot.model.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MessageConverter {

    public MessageDTO convertToDto(Message message) {
        MessageDTO messageDTO = new MessageDTO();
        convertToDto(message, messageDTO);
        return messageDTO;
    }

    public MessageDTO convertToDto(Message message, MessageDTO messageDTO) {
        messageDTO.setMessageID(message.getMessageID());
        messageDTO.setMessage(message.getMessage());
        messageDTO.setSentDate(message.getSentDate());
        messageDTO.setSenderID(message.getSenderUser().getIdUser());
        messageDTO.setRecipientID(message.getRecipientUser().getIdUser());
        messageDTO.setUser1ID(message.getUser1().getIdUser());
        messageDTO.setUser2ID(message.getUser2().getIdUser());
        return messageDTO;
    }

    public Message convertFromDto(MessageDTO messageDTO, User senderUser, User recipientUser, User user1, User user2) {
        Message message = new Message();
        convertFromDto(messageDTO, senderUser, recipientUser, user1, user2, message);
        return message;
    }

    public Message convertFromDto(MessageDTO messageDTO, User senderUser, User recipientUser, User user1, User user2, Message message) {
        message.setMessage(messageDTO.getMessage());
        message.setSenderUser(senderUser);
        message.setRecipientUser(recipientUser);
        message.setUser1(user1);
        message.setUser2(user2);
        message.setSentDate(messageDTO.getSentDate());
        return message;
    }

    public List<MessageDTO> convertListToDtoList(List<Message> messages) {
        List<MessageDTO> messageDTOS = new ArrayList<>();
        messages.forEach(message -> messageDTOS.add(convertToDto(message)));
        return messageDTOS;
    }

}
