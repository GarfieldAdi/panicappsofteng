package com.example.panicappspringboot.service;

import com.example.panicappspringboot.converter.EmContactConverter;
import com.example.panicappspringboot.converter.UserConverter;
import com.example.panicappspringboot.dto.*;
import com.example.panicappspringboot.exception.GlobalExceptionHandler;
import com.example.panicappspringboot.model.EmergencyContact;
import com.example.panicappspringboot.model.Role;
import com.example.panicappspringboot.model.User;
import com.example.panicappspringboot.repository.EmergencyContactRepository;
import com.example.panicappspringboot.repository.RoleRepository;
import com.example.panicappspringboot.repository.UserRepository;
import com.example.panicappspringboot.utils.ImageUtils;
import com.example.panicappspringboot.utils.Token;
import com.example.panicappspringboot.utils.TokenStorage;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.persistence.EntityExistsException;
import javax.persistence.EntityNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private EmergencyContactRepository emContactRepo;

    @Autowired
    private UserConverter userConverter;

    @Autowired
    private EmContactConverter emContactConverter;

    @Autowired
    private TokenStorage tokenStorage;

    @Autowired
    private TwilioSmsSender twilioSmsSender;

    @Autowired
    private SecurityService securityService;

    @Autowired
    private GeoLocationService geoLocationService;

    @Autowired
    private ImageUtils imageUtils;

    @Autowired
    private ImageService imageService;

    public List<UserDTO> findAllUsers() {
        List<User> users = userRepository.findAll();
        return userConverter.convertListToDtoList(users);
    }

    public List<UserDTO> findAllAdminUsers() {
        List<User> users = userRepository.findAllByRole_IdRole(1L);
        List<UserDTO> userDTOS = new ArrayList<>();

        users.forEach(user -> userDTOS.add(userConverter.convertToDto(user)));
        return userDTOS;
    }

    public UserDTO findUserByID(Long id) {
        User user = userRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("User with id " + id + " doesn't exist."));
        return userConverter.convertToDto(user);
    }

    public UserDTO findUserByUsername(String username) {
        User user = userRepository.getUserByUsername(username).orElseThrow(() ->
                new EntityNotFoundException("User with username " + username + " doesn't exist."));
        return userConverter.convertToDto(user);
    }

    @Transactional
    public UserDTO insertUser(UserDTO userDTO) {
        String username = userDTO.getUsername();

        if (userRepository.existsUserByUsername(username)) {
            throw new EntityExistsException("User with " + username + " username already exists!");
        }

        Role role = roleRepository.getRoleByUserRole("Standard");

        userDTO.setFileName(imageService.generateUniqueFileName() + ".jpg");
        User user = userConverter.convertFromDto(userDTO, role);

        userRepository.save(user);
        imageService.saveImageInResources(userDTO);
        return userConverter.convertToDto(user);

    }

    public UserDTO updateUser(UserDTO userDTO, MultipartFile file) {
        User user = userRepository.findById(userDTO.getUserID()).orElseThrow(() ->
                new EntityNotFoundException("User with id: " + userDTO.getUserID() + " doesn't exist. Can't update!"));
        Role role = roleRepository.findById(userDTO.getRoleID()).orElseThrow(() ->
                new EntityNotFoundException("Role with id: " + userDTO.getRoleID() + " doesn't exist. Can't update!"));
        if (file != null) {
            imageService.deleteImageFromResources(userDTO);
            userDTO.setFileName(imageService.generateUniqueFileName() + ".jpg");
            userDTO.setFile(file);
            imageService.saveImageInResources(userDTO);
        }
        userConverter.convertFromDto(userDTO, user, role);

        userRepository.save(user);
        return userConverter.convertToDto(user);
    }

    @Transactional
    public UserDTO deleteUser(UserDTO userDTO) {
        User user = userRepository.findById(userDTO.getUserID()).orElseThrow(() ->
                new EntityNotFoundException("User " + userDTO.getUsername() + " doesn't exist. Can't delete!"));
        userRepository.delete(user);
        imageService.deleteImageFromResources(userDTO);
        return userConverter.convertToDto(user);
    }

    public void sendEmergencySms(Integer priority, NotificationDTO notificationDTO, UserDTO userDTO) {
        Long userID = userDTO.getUserID();
        List<EmergencyContact> emergencyContacts = emContactRepo.findAllByUser_IdUserAndPriority(userID, priority);
        List<EmContactDTO> emContactDTOS = emContactConverter.convertListToDtoList(emergencyContacts);
        GeoLocationDTO geoLocationDTO = geoLocationService.getGeoLocationDTO();

        emContactDTOS.forEach(emContactDTO -> {
            SmsRequest smsRequest = new SmsRequest();
            smsRequest.setPhoneNumber(emContactDTO.getTelNum());
            smsRequest.setMessage(securityService.getUserDTO().getUsername() + " is in danger because of " + notificationDTO.getEmRequestType() + "! You are their emergency contact and see if your acquaintance is okay! Their coordinates are: latitude: " + geoLocationDTO.getLatitude() + " longitude: " + geoLocationDTO.getLongitude());
            twilioSmsSender.sendSms(smsRequest);
        });
    }

    public void generateToken(UserDTO userDTO) {
        String telNum = userDTO.getTelNum();

        User user = userRepository.getUserByTelNum(telNum).orElseThrow(() ->
                new EntityNotFoundException("Tel. nr. " + telNum + " doesn't exist. Can't generate token!"));
        String tokenKey = UUID.randomUUID().toString().substring(0, 5);

        Token token = new Token();
        token.setToken(tokenKey);
        token.setUserID(user.getIdUser());
        token.setCreationalDate(new Date());
        tokenStorage.addTokenInStorage(token);

        SmsRequest smsRequest = new SmsRequest();
        smsRequest.setPhoneNumber(telNum);
        smsRequest.setMessage("The code for the user " + user.getUsername() + " is: " + tokenKey);
        twilioSmsSender.sendSms(smsRequest);
    }

    public void resetPassword(PasswordChangeDTO passwDTO) {
        Token token = tokenStorage.getTokenWithKey(passwDTO.getToken()).orElseThrow(() ->
                new EntityNotFoundException("Wrong or expired token!"));
        User user = userRepository.findById(token.getUserID()).orElseThrow(() ->
                new EntityNotFoundException("User with id: " + token.getUserID() + " doesn't exist. Can't update!"));
        user.setPassword(passwDTO.getPassword());
        userRepository.save(user);
    }

}
