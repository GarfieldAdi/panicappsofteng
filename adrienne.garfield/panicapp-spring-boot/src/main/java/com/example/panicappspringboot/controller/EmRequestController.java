package com.example.panicappspringboot.controller;

import com.example.panicappspringboot.dto.EmContactDTO;
import com.example.panicappspringboot.dto.EmRequestDTO;
import com.example.panicappspringboot.service.EmergencyRequestService;
import com.example.panicappspringboot.service.SecurityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("http://localhost:4200")
@RequestMapping("/emergencyRequests")
public class EmRequestController {

    @Autowired
    EmergencyRequestService emRequestService;

    @Autowired
    SecurityService securityService;

    private static final Logger LOGGER = LoggerFactory.getLogger(EmRequestController.class);

    @GetMapping(path = "/{userID}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<EmRequestDTO>> getEmRequestsOfUser(@PathVariable Long userID) {
        LOGGER.info("Find emergency requests of user with id: " + userID);
        if (!securityService.authorized(userID)) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        List<EmRequestDTO> emRequestDTOs = emRequestService.findEmRequestsOfUserByID(userID);
        LOGGER.info("Found successfully emergency requests of user with id: " + userID);
        return new ResponseEntity<>(emRequestDTOs, HttpStatus.OK);
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmRequestDTO> addRequest(@RequestBody EmRequestDTO emRequestDTO) {
        LOGGER.info("Insert new request");
        if (!securityService.authorized(emRequestDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        EmRequestDTO emRequestDTOResp = emRequestService.insertEmRequest(emRequestDTO);
        LOGGER.info("New request inserted");
        return new ResponseEntity<>(emRequestDTOResp, HttpStatus.CREATED);
    }

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmRequestDTO> updateRequest(@RequestBody EmRequestDTO emRequestDTO) {
        LOGGER.info("Update request with id: {}", emRequestDTO.getEmRequestID());
        if (!securityService.authorized(emRequestDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        EmRequestDTO emRequestDTOResp = emRequestService.updateEmRequest(emRequestDTO);
        LOGGER.info("Update request with id: {}", emRequestDTO.getEmRequestID());
        return new ResponseEntity<>(emRequestDTOResp, HttpStatus.CREATED);
    }

    @DeleteMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<EmRequestDTO> deleteRequest(@RequestBody EmRequestDTO emRequestDTO) {
        LOGGER.info("Delete request with id: {}", emRequestDTO.getEmRequestID());
        if (!securityService.authorized(emRequestDTO.getUserID())) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
        EmRequestDTO emRequestDTOResp = emRequestService.deleteEmRequest(emRequestDTO);
        LOGGER.info("Deleted request with id: {}", emRequestDTO.getEmRequestID());
        return new ResponseEntity<>(emRequestDTOResp, HttpStatus.OK);
    }

}
