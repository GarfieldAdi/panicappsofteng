package com.example.panicappspringboot.repository;

import com.example.panicappspringboot.model.Contact;
import com.example.panicappspringboot.model.ContactType;
import com.example.panicappspringboot.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ContactRepository extends JpaRepository<Contact, Long> {

    boolean existsByUser1AndUser2(User user1, User user2);

    Optional<Contact> findByUser1AndUser2(User user1, User user2);

    List<Contact> findAllByUser1AndContactType(User user1, ContactType contactType);

}
