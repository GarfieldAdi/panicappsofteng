package com.example.panicappspringboot.service;

import com.example.panicappspringboot.dto.SecurityUser;
import com.example.panicappspringboot.model.User;
import com.example.panicappspringboot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class MyUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepo.getUserByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("Error!"));
        return new SecurityUser(user);
    }

}
