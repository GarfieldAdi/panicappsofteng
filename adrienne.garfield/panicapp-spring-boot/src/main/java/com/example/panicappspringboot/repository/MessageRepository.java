package com.example.panicappspringboot.repository;

import com.example.panicappspringboot.model.Message;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface MessageRepository extends JpaRepository<Message, Long> {

    Optional<Message> findFirstByUser1IdUserAndUser2IdUser(Long user1ID, Long user2ID);

    List<Message> getMessagesByUser1IdUserAndUser2IdUser(Long user1ID, Long user2ID);

    void deleteMessagesByUser1IdUserAndUser2IdUser(Long user1ID, Long user2ID);

}
