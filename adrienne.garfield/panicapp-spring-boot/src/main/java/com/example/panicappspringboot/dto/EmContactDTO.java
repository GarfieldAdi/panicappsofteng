package com.example.panicappspringboot.dto;

public class EmContactDTO {

    private Long emContactID;
    private String nameContact;
    private String telNum;
    private Integer priority;
    private Long userID;

    public Long getEmContactID() {
        return emContactID;
    }

    public void setEmContactID(Long emContactID) {
        this.emContactID = emContactID;
    }

    public String getNameContact() {
        return nameContact;
    }

    public void setNameContact(String nameContact) {
        this.nameContact = nameContact;
    }

    public String getTelNum() {
        return telNum;
    }

    public void setTelNum(String telNum) {
        this.telNum = telNum;
    }

    public Integer getPriority() {
        return priority;
    }

    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    public Long getUserID() {
        return userID;
    }

    public void setUserID(Long userID) {
        this.userID = userID;
    }

    @Override
    public String toString() {
        return "EmContactDTO{" +
                "emContactID=" + emContactID +
                ", nameContact='" + nameContact + '\'' +
                ", telNum='" + telNum + '\'' +
                ", priority=" + priority +
                ", userID=" + userID +
                '}';
    }
}
