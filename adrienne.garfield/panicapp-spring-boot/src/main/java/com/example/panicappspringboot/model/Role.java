package com.example.panicappspringboot.model;

import javax.persistence.*;
import java.util.Set;

@Entity(name = "Roles")
@Table(name = "Roles")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDRole")
    private Long idRole;

    @Column(name = "UserRole", length = 200, nullable = false, unique = true)
    private String userRole;

    @OneToMany(mappedBy = "role", cascade = CascadeType.ALL,
            orphanRemoval = true)
    private Set<User> users;

    public Role() {

    }

    public Long getIdRole() {
        return idRole;
    }

    public void setIdRole(Long IDRole) {
        this.idRole = IDRole;
    }

    public String getUserRole() {
        return userRole;
    }

    public void setUserRole(String userRole) {
        this.userRole = userRole;
    }

    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

}
