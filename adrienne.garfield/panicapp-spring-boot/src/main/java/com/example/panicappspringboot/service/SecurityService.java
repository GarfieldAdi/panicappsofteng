package com.example.panicappspringboot.service;

import com.example.panicappspringboot.converter.UserConverter;
import com.example.panicappspringboot.dto.SecurityUser;
import com.example.panicappspringboot.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Set;
import java.util.stream.Collectors;

@Service("userSecurity")
public class SecurityService {

    @Autowired
    UserConverter userConverter;

    private UserDTO userDTO;

    public UserDTO getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        UserDTO userDTOResp = new UserDTO();
        userDTOResp.setUsername(username);
        return userDTOResp;
    }

    public UserDTO getUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        return userConverter.convertToDto(securityUser.getUser());
    }

    public Set<String> getRoles() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        return authentication.getAuthorities().stream()
                .map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
    }

    public boolean authorized(Long userID) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        SecurityUser securityUser = (SecurityUser) authentication.getPrincipal();
        return securityUser.getUser().getIdUser().equals(userID) || getRoles().contains("ROLE_ADMIN");
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTOLogin(SecurityUser securityUser) {
        this.userDTO = userConverter.convertToDto(securityUser.getUser());
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }
}
