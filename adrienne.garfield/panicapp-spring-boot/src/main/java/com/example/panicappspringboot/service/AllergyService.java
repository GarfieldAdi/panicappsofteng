package com.example.panicappspringboot.service;

import com.example.panicappspringboot.converter.AllergyConverter;
import com.example.panicappspringboot.dto.AllergyDTO;
import com.example.panicappspringboot.model.Allergy;
import com.example.panicappspringboot.model.User;
import com.example.panicappspringboot.repository.AllergyRepository;
import com.example.panicappspringboot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class AllergyService {

    @Autowired
    private AllergyRepository allergyRepo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private AllergyConverter allergyConv;

    public List<AllergyDTO> findAllergiesOfUserByID(Long userID) {
        List<Allergy> allergies = allergyRepo.findAllByUser_IdUser(userID);
        return allergyConv.convertListToDtoList(allergies);
    }

    @Transactional
    public AllergyDTO insertAllergy(AllergyDTO allergyDTO) {
        Long userID = allergyDTO.getUserID();

        User user = userRepo.findById(userID).orElseThrow(() ->
                new EntityNotFoundException("User doesn't exist!"));

        Allergy allergy = allergyConv.convertFromDto(allergyDTO, user);

        allergyRepo.save(allergy);
        return allergyConv.convertToDto(allergy);

    }

    public AllergyDTO deleteAllergyByID(AllergyDTO allergyDTO) {
        Long allergyID = allergyDTO.getAllergyID();

        Allergy allergy = allergyRepo.findById(allergyID).orElseThrow(() ->
                new EntityNotFoundException("Cannot delete allergy if it doesn't exist!"));

        allergyRepo.delete(allergy);
        return allergyConv.convertToDto(allergy);
    }

    public AllergyDTO updateAllergy(AllergyDTO allergyDTO) {

        Long allergyID = allergyDTO.getAllergyID();

        Allergy allergy = allergyRepo.findById(allergyID).orElseThrow(() ->
                new EntityNotFoundException("Cannot update allergy if it doesn't exist!"));

        allergy.setAllergyName(allergyDTO.getAllergyName());
        allergy.setComments(allergyDTO.getComments());
        allergy.setSevereness(allergyDTO.getSevereness());
        allergyRepo.save(allergy);

        return allergyConv.convertToDto(allergy);
    }


}
