package com.example.panicappspringboot.model;

import javax.persistence.*;

@Entity(name = "Sicknesses")
public class Sickness {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "IDSickness")
    private Long idSickness;

    @Column(name = "SickName", nullable = false)
    private String sicknessName;

    @Column(name = "Severeness", nullable = false)
    private Integer severeness;

    @Column(name = "Comments", length = 2000)
    private String comments;

    @ManyToOne
    @JoinColumn(name = "UserID", referencedColumnName = "IDUser")
    private User user;

    public Long getIdSickness() {
        return idSickness;
    }

    public void setIdSickness(Long idSickness) {
        this.idSickness = idSickness;
    }

    public String getSicknessName() {
        return sicknessName;
    }

    public void setSicknessName(String sicknessName) {
        this.sicknessName = sicknessName;
    }

    public Integer getSevereness() {
        return severeness;
    }

    public void setSevereness(Integer severeness) {
        this.severeness = severeness;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
